import React, { useContext, useEffect, Suspense, lazy, useState } from "react";
import {
  BrowserRouter as Router,
  Route,
  Routes,
  Navigate,
  useLocation,
  useNavigate,
} from "react-router-dom";
import Header from "./components/Header/Header";
import Footer from "./components/Footer/Footer";
import { AuthContext } from "./components/Context/AuthContext";
import { callAPi } from "./services/UserService";
import ButtonOnTop from "./components/ButtonOnTop/ButtonOnTop";
import ChatBox from "./components/ChatBox/ChatBox";
import { DataContext } from "./components/Context/DataContext";
import { signal } from "@preact/signals-react";
import Manage from "./components/Manage/Manage";
export const menu = signal("news");

const Home = lazy(() => import("./components/Home/Home"));
const Login = lazy(() => import("./components/Admin/Login"));
const Menu = lazy(() => import("./components/MenuAdmin/MenuAdmin"));
const ManagePost = lazy(() => import("./components/ManagePost/ManagePost"));
const CreatePost = lazy(() => import("./components/CreatePost/CreatePost"));
const NewsDetail = lazy(() => import("./components/NewsDetail/NewsDetail"));
const Chat = lazy(() => import("./components/Chat/Chat"));
const ArticleCategory = lazy(() =>
  import("./components/ArticleCategory/ArticleCategory")
);
const ChangePost = lazy(() => import("./components/ChangePost/ChangePost"));
const Setting = lazy(() => import("./components/Setting/Setting"));

function App() {
  const { login, URL, authDispatch } = useContext(AuthContext);
  const location = useLocation();
  const navigate = useNavigate();

  useEffect(() => {
    const token =
      JSON.parse(localStorage.getItem("token")) ||
      JSON.parse(sessionStorage.getItem("token"));
    if (token) {
      getUserByToken(token);
    } else {
      authDispatch({
        type: "SET_LOGIN",
        payload: { state: false },
      });
      if (
        location.pathname === "/admin" ||
        location.pathname === "/admin/setting" ||
        location.pathname === "/admin/createarticle" ||
        location.pathname === "/admin/chat" ||
        location.pathname.startsWith("/admin/editarticle") ||
        location.pathname === "/auth/login"
      ) {
        navigate("/auth/login");
      }
    }
  }, [login.state]);

  const getUserByToken = async (token) => {
    let res = await callAPi("post", `${URL}/auth/verify-jwt`, {
      token: token,
    });
    if (res.status) {
      authDispatch({
        type: "SET_LOGIN",
        payload: { state: true },
      });
      console.log(res)
      authDispatch({
        type: "SET_USER",
        payload: res.username,
      });
      if (location.pathname === "/auth/login") {
        navigate("/admin");
      } else {
        navigate(location.pathname);
      }
    } else {
      authDispatch({
        type: "SET_LOGIN",
        payload: { state: false },
      });
    }
  };

  return (
    <Suspense fallback={<div>Loading...</div>}>
      <Routes>
        {login.state ? (
          <Route path="/admin/*" element={<AdminLayout />} />
        ) : (
          <Route path="/auth/*" element={<AuthRoutes />} />
        )}
        <Route path="/*" element={<HomeRoutes />} />
      </Routes>
    </Suspense>
  );
}

const AdminLayout = () => {
  const { URL, dataDispatch, categories } = useContext(DataContext);
  const getCategories = async () => {
    let res = await callAPi("get", `${URL}/data/get-all-categories`);
    if (!res.status) {
      alert("get categories fail");
      return;
    }
    dataDispatch({
      type: "SET_CATEGORIES",
      payload: res.data,
    });
  };
  const getNavbar = async () => {
    let res = await callAPi("get", `${URL}/data/get-menu`);
    if (!res.status) {
      alert("get navbar fail");
      return;
    }
    dataDispatch({
      type: "SET_HEADER_MENU",
      payload: res.data,
    });
  };
  const getNewsNull = async () => {
    let res = await callAPi("get", `${URL}/data/get-news-by-null-category`);
    if (!res.status) {
      alert("get newsnull fail");
      return;
    }
    dataDispatch({
      type: "SET_NEWS_NULL_CATEGORIES",
      payload: res.data,
    });
  };
  const getNewsHighLights = async () => {
    let res = await callAPi("get", `${URL}/data/get-highlight-news`);
    if (!res.status) {
      alert("get news highlights fail");
      return;
    }
    dataDispatch({
      type: "SET_NEWS_HIGHLIGHTS",
      payload: res.data,
    });
  };

  useEffect(() => {
    getNavbar();
    getNewsNull();
    getCategories();
    getNewsHighLights();
  }, []);
  const getNewsCategory = async () => {
    if (Array.isArray(categories) && categories.length > 0) {
      const promises = categories.map(async (category) => {
        const res = await callAPi(
          "get",
          `${URL}/data/get-news-by-category/${category.id}`
        );
        return { [category.id]: res.data };
      });

      const results = await Promise.all(promises);
      const data = results.reduce((acc, result) => {
        return { ...acc, ...result };
      }, {});
      dataDispatch({
        type: "SET_NEWS_CATEGORIES",
        payload: data,
      });
    }
  };
  useEffect(() => {
    getNewsCategory();
  }, [categories]);
  return (
    <div className="DAT_Admin">
      <Menu />
      <Suspense fallback={<div>Loading...</div>}>
        <Routes>
          <Route path="/" element={<ManagePost />} />
          <Route path="/manage" element={<Manage />} />
          <Route path="setting" element={<Setting />} />
          <Route path="createarticle" element={<CreatePost />} />
          <Route path="chat" element={<Chat />} />
          <Route path="editarticle/:idPost" element={<ChangePost />} />
          <Route path="*" element={<Navigate to="/admin" replace />} />
        </Routes>
      </Suspense>
    </div>
  );
};

const AuthRoutes = () => {
  return (
    <Suspense fallback={<div>Loading...</div>}>
      <Routes>
        <Route path="login" element={<Login />} />
        <Route path="*" element={<Navigate to="/auth/login" replace />} />
      </Routes>
    </Suspense>
  );
};

const HomeRoutes = () => {
  const { URL, dataDispatch } = useContext(DataContext);
  const [data, setData] = useState({});
  const [menu, setMenu] = useState([]);
  const getNews = async () => {
    let res = await callAPi("get", `${URL}/data/get-news`);
    if (!res.status) {
      return;
    }
    if (res.status) {
      let temp = res.data;
      setData(temp);
    }
  };

  const getMenuNavbar = async () => {
    let res = await callAPi("get", `${URL}/data/get-menu`);
    if (!res.status) {
      return;
    }
    setMenu(res.data);
  };

  useEffect(() => {
    getNews();
    getMenuNavbar();
  }, []);

  useEffect(() => {
    dataDispatch({
      type: "SET_HIGHLIGHTS_BY_ALL",
      payload: data.highlight,
    });
    dataDispatch({
      type: "SET_HIGHLIGHTS_BY_CATEGORIES",
      payload: data.category,
    });
    dataDispatch({
      type: "SET_HEADER_MENU",
      payload: menu,
    });
  }, [data, menu]);
  return (
    <div className="DAT_Page">
      <Header />
      <Suspense fallback={<div>Loading...</div>}>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/article-detail/:url" element={<NewsDetail />} />
          <Route path="article-category/:url" element={<ArticleCategory />} />
          <Route path="/auth/login" element={<Login />} />
          <Route path="*" element={<Navigate to="/" replace />} />
        </Routes>
      </Suspense>
      <ButtonOnTop />
      <ChatBox />
      <Footer />
    </div>
  );
};

export default App;
