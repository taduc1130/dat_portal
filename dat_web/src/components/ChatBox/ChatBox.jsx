import "./ChatBox.scss";
import { FaFacebookMessenger } from "react-icons/fa";
import { FiX } from "react-icons/fi";
import { FaRegFaceSmile } from "react-icons/fa6";
import { IoSend } from "react-icons/io5";
import { useState } from "react";
const ChatBox = () => {
  const [isOpen, setIsOpen] = useState(false);
  return (
    <>
      <div className="ChatBox">
        <div className="ChatBox_Icon" onClick={() => setIsOpen(!isOpen)}>
          <FaFacebookMessenger />
          <div className="ChatBox_Icon_Number">1</div>
        </div>
        <div className={`ChatBox_Pop ${isOpen ? "Show" : ""}`}>
          <div className="ChatBox_Pop_Info">
            <div className="ChatBox_Pop_Info_Logo">
              <img src="/icons/user.png" alt="" />
            </div>
            <div className="ChatBox_Pop_Info_Content">
              <div className="ChatBox_Pop_Info_Content_Name">
                How can we help?
              </div>
              <div className="ChatBox_Pop_Info_Content_Status">
                <div className="ChatBox_Pop_Info_Content_Status_Dot"></div>
                <div className="ChatBox_Pop_Info_Content_Status_Text">
                  We reply immediately
                </div>
              </div>
            </div>
            <div
              className="ChatBox_Pop_Info_Btn"
              onClick={() => setIsOpen(false)}
            >
              <FiX />
            </div>
          </div>
          <div className="ChatBox_Pop_Message"></div>
          <div className="ChatBox_Pop_Chat">
            <div className="ChatBox_Pop_Chat_Icon">
              <FaRegFaceSmile />
            </div>
            <div className="ChatBox_Pop_Chat_Input">
              <input type="text" placeholder="Aa..." />
            </div>
            <div className="ChatBox_Pop_Chat_Icon">
              <IoSend />
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
export default ChatBox;
