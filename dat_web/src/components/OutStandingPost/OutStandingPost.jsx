import { useContext } from "react";
import "./OutStandingPost.scss";
import { useNavigate } from "react-router-dom";
import { AuthContext } from "../Context/AuthContext";
import { BrowserView, MobileView } from "react-device-detect";

import {
  extractAfterDateTimeSuffix,
  extractBeforeDateTimeSuffix,
  formatDateToDDMMYYYY,
} from "../Format/Format";
const OutStandingPost = ({ data }) => {
  const navigate = useNavigate("");
  const { URL } = useContext(AuthContext);
  return (
    <>
      <BrowserView className="OutStandingPost">
          <div className="OutStandingPost_Container">
            {data ? (
              data.length === 3 ? (
                <div className="OutStandingPost_Container_Style3">
                  <div className="OutStandingPost_Container_Style3_Left">
                    <div
                      className="OutStandingPost_Container_Style3_Left_Image"
                      style={{
                        backgroundImage: `url("${URL}/images/${extractAfterDateTimeSuffix(
                          data[0].title
                        )}/${data[0].image_url}")`,
                      }}
                    >
                      <div
                        className="OutStandingPost_Container_Style3_Left_Image_Content"
                        style={{
                          backgroundImage: `url("${URL}/images/${extractAfterDateTimeSuffix(
                            data[0].title
                          )}/${data[0].image_url}")`,
                        }}
                        onClick={() =>
                          navigate(`/article-detail/${data[0].id}`)
                        }
                      >
                        <div className="OutStandingPost_Container_Style3_Left_Image_Content_Fade"></div>
                        <div className="OutStandingPost_Container_Style3_Left_Image_Content_Title">
                          {extractBeforeDateTimeSuffix(data[0].title)}
                        </div>
                        <div className="OutStandingPost_Container_Style3_Left_Image_Content_Desc">
                          {data[0].content}
                        </div>
                        <div className="OutStandingPost_Container_Style3_Left_Image_Content_Date">
                          {formatDateToDDMMYYYY(data[0].published_date)}
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="OutStandingPost_Container_Style3_Right">
                    <div className="OutStandingPost_Container_Style3_Right_Item">
                      <div
                        className="OutStandingPost_Container_Style3_Right_Item_Image"
                        style={{
                          backgroundImage: `url("${URL}/images/${extractAfterDateTimeSuffix(
                            data[1].title
                          )}/${data[1].image_url}")`,
                        }}
                      >
                        <div
                          className="OutStandingPost_Container_Style3_Right_Item_Image_Content"
                          style={{
                            backgroundImage: `url("${URL}/images/${extractAfterDateTimeSuffix(
                              data[1].title
                            )}/${data[1].image_url}")`,
                          }}
                        ></div>
                      </div>
                      <div className="OutStandingPost_Container_Style3_Right_Item_Content">
                        <div
                          className="OutStandingPost_Container_Style3_Right_Item_Content_Title"
                          onClick={() =>
                            navigate(`/article-detail/${data[1].id}`)
                          }
                        >
                          {extractBeforeDateTimeSuffix(data[1].title)}
                        </div>
                        <div className="OutStandingPost_Container_Style3_Right_Item_Content_Desc">
                          {data[1].content}
                        </div>
                        <div className="OutStandingPost_Container_Style3_Right_Item_Content_Date">
                          {formatDateToDDMMYYYY(data[1].published_date)}
                        </div>
                      </div>
                    </div>
                    <div className="OutStandingPost_Container_Style3_Right_Item">
                      <div
                        className="OutStandingPost_Container_Style3_Right_Item_Image"
                        style={{
                          backgroundImage: `url("${URL}/images/${extractAfterDateTimeSuffix(
                            data[2].title
                          )}/${data[2].image_url}")`,
                        }}
                      >
                        <div
                          className="OutStandingPost_Container_Style3_Right_Item_Image_Content"
                          style={{
                            backgroundImage: `url("${URL}/images/${extractAfterDateTimeSuffix(
                              data[2].title
                            )}/${data[2].image_url}")`,
                          }}
                        ></div>
                      </div>
                      <div className="OutStandingPost_Container_Style3_Right_Item_Content">
                        <div
                          className="OutStandingPost_Container_Style3_Right_Item_Content_Title"
                          onClick={() =>
                            navigate(`/article-detail/${data[2].id}`)
                          }
                        >
                          {extractBeforeDateTimeSuffix(data[2].title)}
                        </div>
                        <div className="OutStandingPost_Container_Style3_Right_Item_Content_Desc">
                          {data[2].content}
                        </div>
                        <div className="OutStandingPost_Container_Style3_Right_Item_Content_Date">
                          {formatDateToDDMMYYYY(data[2].published_date)}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              ) : data.length === 2 ? (
                <div className="OutStandingPost_Container_Style2">
                  {data.map((item, index) => (
                    <div
                      className="OutStandingPost_Container_Style2_Item"
                      key={index}
                    >
                      <div
                        className="OutStandingPost_Container_Style2_Item_Image"
                        style={{
                          backgroundImage: `url("${URL}/images/${extractAfterDateTimeSuffix(
                            item.title
                          )}/${item.image_url}")`,
                        }}
                      >
                        <div
                          className="OutStandingPost_Container_Style2_Item_Image_Content"
                          style={{
                            backgroundImage: `url("${URL}/images/${extractAfterDateTimeSuffix(
                              item.title
                            )}/${item.image_url}")`,
                          }}
                          onClick={() => navigate(`/article-detail/${item.id}`)}
                        >
                          <div className="OutStandingPost_Container_Style2_Item_Image_Content_Fade"></div>
                          <div className="OutStandingPost_Container_Style2_Item_Image_Content_Title">
                            {extractBeforeDateTimeSuffix(item.title)}
                          </div>

                          <div className="OutStandingPost_Container_Style2_Item_Image_Content_Desc">
                            {item.content}
                          </div>
                          <div className="OutStandingPost_Container_Style2_Item_Image_Content_Date">
                            {formatDateToDDMMYYYY(item.published_date)}
                          </div>
                        </div>
                      </div>
                    </div>
                  ))}
                </div>
              ) : data.length === 1 ? (
                <div className="OutStandingPost_Container_Style">
                  <div
                    className="OutStandingPost_Container_Style_Item"
                    style={{
                      backgroundImage: `url("${URL}/images/${extractAfterDateTimeSuffix(
                        data[0].title
                      )}/${data[0].image_url}")`,
                    }}
                  >
                    <div
                      className="OutStandingPost_Container_Style_Item_Content"
                      style={{
                        backgroundImage: `url("${URL}/images/${extractAfterDateTimeSuffix(
                          data[0].title
                        )}/${data[0].image_url}")`,
                      }}
                      onClick={() => navigate(`/article-detail/${data[0].id}`)}
                    >
                      <div className="OutStandingPost_Container_Style_Item_Content_Fade"></div>
                      <div className="OutStandingPost_Container_Style_Item_Content_Title">
                        {extractBeforeDateTimeSuffix(data[0].title)}
                      </div>
                      <div className="OutStandingPost_Container_Style_Item_Content_Desc">
                        {data[0].content}
                      </div>
                      <div className="OutStandingPost_Container_Style_Item_Content_Date">
                        {formatDateToDDMMYYYY(data[0].published_date)}
                      </div>
                    </div>
                  </div>
                </div>
              ) : (
                <></>
              )
            ) : (
              <></>
            )}
          </div>
      </BrowserView>
      <MobileView>
      <div className="Mobile_OutStandingPost">
  <div className="Mobile_OutStandingPost_Container">
    {data ? (
      data.length === 3 ? (
        <div className="Mobile_OutStandingPost_Container_Style3">
          <div className="Mobile_OutStandingPost_Container_Style3_Left">
            <div
              className="Mobile_OutStandingPost_Container_Style3_Left_Image"
              style={{
                backgroundImage: `url("${URL}/images/${extractAfterDateTimeSuffix(
                  data[0].title
                )}/${data[0].image_url}")`,
              }}
            >
              <div
                className="Mobile_OutStandingPost_Container_Style3_Left_Image_Content"
                style={{
                  backgroundImage: `url("${URL}/images/${extractAfterDateTimeSuffix(
                    data[0].title
                  )}/${data[0].image_url}")`,
                }}
                onClick={() => navigate(`/article-detail/${data[0].id}`)}
              >
                <div className="Mobile_OutStandingPost_Container_Style3_Left_Image_Content_Fade"></div>
                <div className="Mobile_OutStandingPost_Container_Style3_Left_Image_Content_Title">
                  {extractBeforeDateTimeSuffix(data[0].title)}
                </div>
                <div className="Mobile_OutStandingPost_Container_Style3_Left_Image_Content_Desc">
                  {data[0].content}
                </div>
                <div className="Mobile_OutStandingPost_Container_Style3_Left_Image_Content_Date">
                  {formatDateToDDMMYYYY(data[0].published_date)}
                </div>
              </div>
            </div>
          </div>
          <div className="Mobile_OutStandingPost_Container_Style3_Right">
            <div className="Mobile_OutStandingPost_Container_Style3_Right_Item">
              <div
                className="Mobile_OutStandingPost_Container_Style3_Right_Item_Image"
                style={{
                  backgroundImage: `url("${URL}/images/${extractAfterDateTimeSuffix(
                    data[1].title
                  )}/${data[1].image_url}")`,
                }}
              >
                <div
                  className="Mobile_OutStandingPost_Container_Style3_Right_Item_Image_Content"
                  style={{
                    backgroundImage: `url("${URL}/images/${extractAfterDateTimeSuffix(
                      data[1].title
                    )}/${data[1].image_url}")`,
                  }}
                ></div>
              </div>
              <div className="Mobile_OutStandingPost_Container_Style3_Right_Item_Content">
                <div
                  className="Mobile_OutStandingPost_Container_Style3_Right_Item_Content_Title"
                  onClick={() =>
                    navigate(`/article-detail/${data[1].id}`)
                  }
                >
                  {extractBeforeDateTimeSuffix(data[1].title)}
                </div>
                <div className="Mobile_OutStandingPost_Container_Style3_Right_Item_Content_Desc">
                  {data[1].content}
                </div>
                <div className="Mobile_OutStandingPost_Container_Style3_Right_Item_Content_Date">
                  {formatDateToDDMMYYYY(data[1].published_date)}
                </div>
              </div>
            </div>
            <div className="Mobile_OutStandingPost_Container_Style3_Right_Item">
              <div
                className="Mobile_OutStandingPost_Container_Style3_Right_Item_Image"
                style={{
                  backgroundImage: `url("${URL}/images/${extractAfterDateTimeSuffix(
                    data[2].title
                  )}/${data[2].image_url}")`,
                }}
              >
                <div
                  className="Mobile_OutStandingPost_Container_Style3_Right_Item_Image_Content"
                  style={{
                    backgroundImage: `url("${URL}/images/${extractAfterDateTimeSuffix(
                      data[2].title
                    )}/${data[2].image_url}")`,
                  }}
                ></div>
              </div>
              <div className="Mobile_OutStandingPost_Container_Style3_Right_Item_Content">
                <div
                  className="Mobile_OutStandingPost_Container_Style3_Right_Item_Content_Title"
                  onClick={() =>
                    navigate(`/article-detail/${data[2].id}`)
                  }
                >
                  {extractBeforeDateTimeSuffix(data[2].title)}
                </div>
                <div className="Mobile_OutStandingPost_Container_Style3_Right_Item_Content_Desc">
                  {data[2].content}
                </div>
                <div className="Mobile_OutStandingPost_Container_Style3_Right_Item_Content_Date">
                  {formatDateToDDMMYYYY(data[2].published_date)}
                </div>
              </div>
            </div>
          </div>
        </div>
      ) : data.length === 2 ? (
        <div className="Mobile_OutStandingPost_Container_Style2">
          {data.map((item, index) => (
            <div
              className="Mobile_OutStandingPost_Container_Style2_Item"
              key={index}
            >
              <div
                className="Mobile_OutStandingPost_Container_Style2_Item_Image"
                style={{
                  backgroundImage: `url("${URL}/images/${extractAfterDateTimeSuffix(
                    item.title
                  )}/${item.image_url}")`,
                }}
              >
                <div
                  className="Mobile_OutStandingPost_Container_Style2_Item_Image_Content"
                  style={{
                    backgroundImage: `url("${URL}/images/${extractAfterDateTimeSuffix(
                      item.title
                    )}/${item.image_url}")`,
                  }}
                  onClick={() => navigate(`/article-detail/${item.id}`)}
                >
                  <div className="Mobile_OutStandingPost_Container_Style2_Item_Image_Content_Fade"></div>
                  <div className="Mobile_OutStandingPost_Container_Style2_Item_Image_Content_Title">
                    {extractBeforeDateTimeSuffix(item.title)}
                  </div>

                  <div className="Mobile_OutStandingPost_Container_Style2_Item_Image_Content_Desc">
                    {item.content}
                  </div>
                  <div className="Mobile_OutStandingPost_Container_Style2_Item_Image_Content_Date">
                    {formatDateToDDMMYYYY(item.published_date)}
                  </div>
                </div>
              </div>
            </div>
          ))}
        </div>
      ) : data.length === 1 ? (
        <div className="Mobile_OutStandingPost_Container_Style">
          <div
            className="Mobile_OutStandingPost_Container_Style_Item"
            style={{
              backgroundImage: `url("${URL}/images/${extractAfterDateTimeSuffix(
                data[0].title
              )}/${data[0].image_url}")`,
            }}
          >
            <div
              className="Mobile_OutStandingPost_Container_Style_Item_Content"
              style={{
                backgroundImage: `url("${URL}/images/${extractAfterDateTimeSuffix(
                  data[0].title
                )}/${data[0].image_url}")`,
              }}
              onClick={() => navigate(`/article-detail/${data[0].id}`)}
            >
              <div className="Mobile_OutStandingPost_Container_Style_Item_Content_Fade"></div>
              <div className="Mobile_OutStandingPost_Container_Style_Item_Content_Title">
                {extractBeforeDateTimeSuffix(data[0].title)}
              </div>
              <div className="Mobile_OutStandingPost_Container_Style_Item_Content_Desc">
                {data[0].content}
              </div>
              <div className="Mobile_OutStandingPost_Container_Style_Item_Content_Date">
                {formatDateToDDMMYYYY(data[0].published_date)}
              </div>
            </div>
          </div>
        </div>
      ) : (
        <></>
      )
    ) : (
      <></>
    )}
  </div>
</div>

      </MobileView>
    </>
  );
};

export default OutStandingPost;
