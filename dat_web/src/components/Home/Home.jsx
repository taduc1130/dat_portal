import { useContext } from "react";
import Banner from "../Banner/Banner";
import OutStandingPost from "../OutStandingPost/OutStandingPost";
import "./Home.scss";
import Main from "../Main/Main";
import SimpleSlider from "../SimpleSlider/SimpleSlider";
import { DataContext } from "../Context/DataContext";
import { useNavigate } from "react-router-dom";
import {
  BrowserView,
  MobileView,
} from "react-device-detect";
const Home = () => {
  const navigate = useNavigate("");
  const { highlightAll, highlightCategories } = useContext(DataContext);
  return (
    <>
      <BrowserView className="Home">
          <div className="Home_Banner">
            <Banner />
          </div>
          <div className="Home_Container">
            <Main />
            {highlightAll && highlightAll.length > 0 ? (
              <SimpleSlider data={highlightAll} />
            ) : (
              <></>
            )}
            {highlightCategories && highlightCategories.length > 0 ? (
              highlightCategories.map(
                (item, index) =>
                  item.data && (
                    <div className="Home_Container_OutStandingPost" key={index}>
                      <div className="Home_Container_OutStandingPost_Heading">
                        <div
                          onClick={() =>
                            navigate(`/article-category/${item.id_category}`)
                          }
                        >
                          {item.name_category}
                        </div>
                      </div>
                      <OutStandingPost data={item.data} />
                    </div>
                  )
              )
            ) : (
              <></>
            )}
          </div>
      </BrowserView>
      <MobileView className="Mobile_Home">
          <div className="Mobile_Home_Banner">
            <Banner />
          </div>
          <div className="Mobile_Home_Container">
            <Main />
            {highlightAll && highlightAll.length > 0 ? (
              <SimpleSlider data={highlightAll} />
            ) : (
              <></>
            )}
            {highlightCategories && highlightCategories.length > 0 ? (
              highlightCategories.map(
                (item, index) =>
                  item.data && (
                    <div className="Mobile_Home_Container_OutStandingPost" key={index}>
                      <div className="Mobile_Home_Container_OutStandingPost_Heading">
                        <div
                          onClick={() =>
                            navigate(`/article-category/${item.id_category}`)
                          }
                        >
                          {item.name_category}
                        </div>
                      </div>
                      <OutStandingPost data={item.data} />
                    </div>
                  )
              )
            ) : (
              <></>
            )}
          </div>
      </MobileView>
    </>
  );
};

export default Home;
