import "./CreatePost.scss";
import { useCallback, useContext, useEffect, useRef, useState } from "react";
import News from "../News/News";
import { callAPi } from "../../services/UserService";
import { useNavigate } from "react-router-dom";
import { RiUploadCloud2Line } from "react-icons/ri";
import { FiArrowRight, FiEye, FiX } from "react-icons/fi";
import { FiArrowLeft } from "react-icons/fi";
import { AuthContext } from "../Context/AuthContext";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import {
  ClassicEditor,
  SimpleUploadAdapter,
  AccessibilityHelp,
  Alignment,
  Autoformat,
  AutoImage,
  AutoLink,
  Autosave,
  BlockQuote,
  Bold,
  Code,
  CodeBlock,
  Essentials,
  FindAndReplace,
  FontBackgroundColor,
  FontColor,
  FontFamily,
  FontSize,
  FullPage,
  GeneralHtmlSupport,
  Heading,
  Highlight,
  HtmlComment,
  HtmlEmbed,
  ImageBlock,
  ImageCaption,
  ImageInline,
  ImageInsert,
  ImageInsertViaUrl,
  ImageResize,
  ImageStyle,
  ImageTextAlternative,
  ImageToolbar,
  ImageUpload,
  Indent,
  IndentBlock,
  Italic,
  Link,
  LinkImage,
  List,
  ListProperties,
  Markdown,
  MediaEmbed,
  Paragraph,
  PasteFromMarkdownExperimental,
  PasteFromOffice,
  PictureEditing,
  RemoveFormat,
  SelectAll,
  ShowBlocks,
  SourceEditing,
  SpecialCharacters,
  SpecialCharactersArrows,
  SpecialCharactersCurrency,
  SpecialCharactersEssentials,
  SpecialCharactersLatin,
  SpecialCharactersMathematical,
  SpecialCharactersText,
  Strikethrough,
  Style,
  Subscript,
  Superscript,
  Table,
  TableCaption,
  TableCellProperties,
  TableColumnResize,
  TableProperties,
  TableToolbar,
  TextPartLanguage,
  TextTransformation,
  TodoList,
  Underline,
  Undo,
} from "ckeditor5";
import "ckeditor5/ckeditor5.css";
import { formatStringWithDatTime } from "../Format/Format";
const CreatePost = () => {
  const navigate = useNavigate();
  const [isOpen, setIsOpen] = useState(false);
  const { URL } = useContext(AuthContext);
  const c_htmlData = useRef("");
  const editorContainerRef = useRef(null);
  const editorRef = useRef(null);
  const [isLayoutReady, setIsLayoutReady] = useState(false);
  const editorInstanceRef = useRef();
  const [editorData, setEditorData] = useState("");

  const [imageObj, setImageObj] = useState(null);
  const [imageSrc, setImageSrc] = useState("");
  const [categories, c_setCategories] = useState("");

  const [c_title, c_setTitle] = useState("");
  const [c_description, c_setDescription] = useState("");
  const [c_imageName, c_setImageName] = useState("");
  const [c_category, c_setCategory] = useState([]);

  const [activeIndexes, setActiveIndexes] = useState([]);

  let editorConfig = {
    toolbar: {
      items: [
        "undo",
        "redo",
        "|",
        "sourceEditing",
        "showBlocks",
        "findAndReplace",
        "selectAll",
        "textPartLanguage",
        "|",
        "heading",
        "style",
        "|",
        "fontSize",
        "fontFamily",
        "fontColor",
        "fontBackgroundColor",
        "|",
        "bold",
        "italic",
        "underline",
        "strikethrough",
        "subscript",
        "superscript",
        "code",
        "removeFormat",
        "|",
        "specialCharacters",
        "link",
        "insertImage",
        "mediaEmbed",
        "insertTable",
        "highlight",
        "blockQuote",
        "codeBlock",
        "htmlEmbed",
        "|",
        "alignment",
        "|",
        "bulletedList",
        "numberedList",
        "todoList",
        "indent",
        "outdent",
        "|",
        "accessibilityHelp",
      ],
      shouldNotGroupWhenFull: true,
    },
    plugins: [
      AccessibilityHelp,
      SimpleUploadAdapter,
      Alignment,
      Autoformat,
      AutoImage,
      AutoLink,
      Autosave,
      BlockQuote,
      Bold,
      Code,
      CodeBlock,
      Essentials,
      FindAndReplace,
      FontBackgroundColor,
      FontColor,
      FontFamily,
      FontSize,
      FullPage,
      GeneralHtmlSupport,
      Heading,
      Highlight,
      HtmlComment,
      HtmlEmbed,
      ImageBlock,
      ImageCaption,
      ImageInline,
      ImageInsert,
      ImageInsertViaUrl,
      ImageResize,
      ImageStyle,
      ImageTextAlternative,
      ImageToolbar,
      ImageUpload,
      Indent,
      IndentBlock,
      Italic,
      Link,
      LinkImage,
      List,
      ListProperties,
      Markdown,
      MediaEmbed,
      Paragraph,
      PasteFromMarkdownExperimental,
      PasteFromOffice,
      PictureEditing,
      RemoveFormat,
      SelectAll,
      ShowBlocks,
      SourceEditing,
      SpecialCharacters,
      SpecialCharactersArrows,
      SpecialCharactersCurrency,
      SpecialCharactersEssentials,
      SpecialCharactersLatin,
      SpecialCharactersMathematical,
      SpecialCharactersText,
      Strikethrough,
      Style,
      Subscript,
      Superscript,
      Table,
      TableCaption,
      TableCellProperties,
      TableColumnResize,
      TableProperties,
      TableToolbar,
      TextPartLanguage,
      TextTransformation,
      TodoList,
      Underline,
      Undo,
    ],
    balloonToolbar: [
      "bold",
      "italic",
      "|",
      "link",
      "insertImage",
      "|",
      "bulletedList",
      "numberedList",
    ],
    blockToolbar: [
      "fontSize",
      "fontColor",
      "fontBackgroundColor",
      "|",
      "bold",
      "italic",
      "|",
      "link",
      "insertImage",
      "insertTable",
      "|",
      "bulletedList",
      "numberedList",
    ],
    fontFamily: {
      supportAllValues: true,
    },
    fontSize: {
      options: [10, 12, 14, "default", 18, 20, 22],
      supportAllValues: true,
    },
    heading: {
      options: [
        {
          model: "paragraph",
          title: "Paragraph",
          class: "ck-heading_paragraph",
        },
        {
          model: "heading1",
          view: "h1",
          title: "Heading 1",
          class: "ck-heading_heading1",
        },
        {
          model: "heading2",
          view: "h2",
          title: "Heading 2",
          class: "ck-heading_heading2",
        },
        {
          model: "heading3",
          view: "h3",
          title: "Heading 3",
          class: "ck-heading_heading3",
        },
        {
          model: "heading4",
          view: "h4",
          title: "Heading 4",
          class: "ck-heading_heading4",
        },
        {
          model: "heading5",
          view: "h5",
          title: "Heading 5",
          class: "ck-heading_heading5",
        },
        {
          model: "heading6",
          view: "h6",
          title: "Heading 6",
          class: "ck-heading_heading6",
        },
      ],
    },
    htmlSupport: {
      allow: [
        {
          name: /^.*$/,
          styles: true,
          attributes: true,
          classes: true,
        },
      ],
    },
    image: {
      toolbar: [
        "toggleImageCaption",
        "imageTextAlternative",
        "|",
        "imageStyle:inline",
        "imageStyle:wrapText",
        "imageStyle:breakText",
        "|",
        "resizeImage",
      ],
    },
    initialData: "",
    link: {
      addTargetToExternalLinks: true,
      defaultProtocol: "http://",
      decorators: {
        toggleDownloadable: {
          mode: "manual",
          label: "Downloadable",
          attributes: {
            download: "file",
          },
        },
      },
    },
    list: {
      properties: {
        styles: true,
        startIndex: true,
        reversed: true,
      },
    },
    menuBar: {
      isVisible: true,
    },
    placeholder: "Type or paste your content here!",
    table: {
      contentToolbar: [
        "tableColumn",
        "tableRow",
        "mergeTableCells",
        "tableProperties",
        "tableCellProperties",
      ],
    },
    simpleUpload: {
      uploadUrl: `${URL}/data/upload-image-temp`,
      withCredentials: true,
      headers: {
        session_id: sessionStorage.getItem("sessionId"),
      },
      onUploadComplete: (response) => {
        console.log("Uploaded image response:", response);
      },
    },
  };
  useEffect(() => {
    const getCategories = async () => {
      let res = await callAPi("get", `${URL}/data/get-all-categories`);
      if (res) {
        c_setCategories(res.data);
      }
    };
    getCategories();
  }, []);

  const handleSaveCategory = (item) => {
    if (!c_category.includes(item.id)) {
      c_setCategory((prevCategories) => {
        const newCategories = [...prevCategories, item.id];
        return newCategories;
      });
    } else {
      c_setCategory((prevCategories) => {
        const newCategories = prevCategories.filter((id) => id !== item.id);
        return newCategories;
      });
    }
  };

  const handleClick = (index, item) => {
    setActiveIndexes((prevIndexes) => {
      if (prevIndexes.includes(index)) {
        // Nếu item đã active, click lần thứ hai sẽ tắt active
        return prevIndexes.filter((i) => i !== index);
      } else {
        // Nếu item chưa active, click sẽ active item đó
        return [...prevIndexes, index];
      }
    });
    handleSaveCategory(item);
  };

  const handleDeleteImage = () => {
    setImageObj(null);
    setImageSrc("");
    c_setImageName("");
  };

  const handleOnChangeImage = async (e) => {
    const files = e.target.files;
    if (files.length > 0) {
      let fileObj = files[0];
      let fileType = fileObj.type.split("/")[0];
      if (fileType === "image") {
        setImageObj(fileObj);
      } else {
        alert("Vui lòng nhập ảnh có định dạng là ảnh");
      }
    } else {
      alert("Vui lòng nhập ảnh");
    }
  };

  useEffect(() => {
    if (imageObj != null) {
      let originalName = imageObj.name;
      let lastDotIndex = originalName.lastIndexOf(".");
      let nameWithoutExtension = originalName.substring(0, lastDotIndex);
      let extension = originalName.substring(lastDotIndex);

      let newFileName = `${nameWithoutExtension}__bia${extension}`;

      let newFile = new File([imageObj], newFileName, { type: imageObj.type });
      const getImage = async () => {
        const formData = new FormData();
        formData.append("upload", newFile);

        let res = await callAPi(
          "post",
          `${URL}/data/upload-image-temp`,
          formData,
          {
            session_id: sessionStorage.getItem("sessionId"),
          }
        );
        setImageSrc(res.url);
        c_setImageName(res.imageName);
      };
      getImage();
    }
  }, [imageObj]);

  useEffect(() => {
    fetchSessionId();
  }, []);

  const handleSetSessionId = () => {
    sessionStorage.removeItem("sessionId");
    fetchSessionId();
  };
  const fetchSessionId = async () => {
    try {
      let res = await callAPi(
        "post",
        `http://172.31.8.230:3001/data/get-session-id`,
        ""
      );
      sessionStorage.setItem("sessionId", res.sessionId);
    } catch (error) {
      console.log("Error saving post:", error);
    }
  };

  const handleGetHtmlSource = () => {
    if (editorInstanceRef.current) {
      c_htmlData.current = editorInstanceRef.current.getData();
    }
  };

  const isImgInHtml = async () => {
    // Kiểm tra xem htmlData.current có chứa định dạng ảnh hay không
    const containsImage = /<img[^>]+src="([^">]+)"/.test(c_htmlData.current);
    return containsImage;
  };

  const handleSavePost = async () => {
    if (c_title === "") {
      alert("Vui lòng nhập Tiêu đề bài viết!");
      return;
    }
    if (c_description === "") {
      alert("Vui lòng nhập mô tả bài viết!");
      return;
    }
    if (c_imageName === "") {
      alert("Vui lòng nhập ảnh bài viết!");
      return;
    }
    if (c_category.length === 0) {
      alert("Vui lòng chọn danh mục bài viết!");
      return;
    }
    handleGetHtmlSource();
    let res = await isImgInHtml();
    let formatTitle = formatStringWithDatTime(c_title);
    const dataArray = [c_imageName, formatTitle, c_description, "", c_category];
    const body = {
      content: c_htmlData.current,
      sessionId: sessionStorage.getItem("sessionId"),
      dataArray: dataArray,
    };
    try {
      let res = await callAPi("post", `${URL}/data/save-post`, body, {
        headers: {
          "Content-Type": "application/json",
        },
      });
      if (res.sessionId === sessionStorage.getItem("sessionId")) {
        if (res.result.status) {
          setImageSrc("");
          c_setImageName("");
          setImageObj(null);
          c_setTitle("");
          c_setCategory([]);
          c_setDescription("");
          setEditorData("");
          navigate("/admin/");
          setIsLayoutReady(false);
          alert(res.result.message);
        } else {
          setIsLayoutReady(false);
          alert(res.result.message);
        }
      }
    } catch (error) {
      console.error("Error saving post:", error);
    }
    handleSetSessionId();
    setEditorData("");
  };

  const handleKeyDown = useCallback((event) => {
    if (event.key === "Enter") {
      event.preventDefault();
    }
  }, []);
  return (
    <>
      <div className="CreatePost">
        {isOpen ? (
          <div className="CreatePost_Pop">
            <div className="CreatePost_Pop_Container">
              <div
                onClick={() => setIsOpen(false)}
                className="CreatePost_Pop_Container_Btn"
              >
                <FiX />
              </div>
              <News image={imageSrc} title={c_title} desc={c_description} />
            </div>
          </div>
        ) : (
          <></>
        )}
        {!isLayoutReady ? (
          <div className="CreatePost_Container">
            <div className="CreatePost_Container_Header">
              <div
                className="CreatePost_Container_Header_Back"
                onClick={() => navigate("/admin/managepost")}
              >
                <FiArrowLeft /> <span>Quay lại</span>
              </div>
              <div className="CreatePost_Container_Header_Heading">
                Đăng bài viết
              </div>
            </div>
            <div className="CreatePost_Container_Content">
              <div className="CreatePost_Container_Content_Item">
                <div className="CreatePost_Container_Content_Item_SubItem">
                  <div className="CreatePost_Container_Content_Item_SubItem_Title">
                    Tiêu đề bài viết :
                  </div>
                  <div className="CreatePost_Container_Content_Item_SubItem_Input">
                    <textarea
                      onKeyDown={handleKeyDown}
                      placeholder="Nhập..."
                      value={c_title}
                      onChange={(e) => c_setTitle(e.target.value)}
                    ></textarea>
                  </div>
                </div>
                <div className="CreatePost_Container_Content_Item_SubItem">
                  <div className="CreatePost_Container_Content_Item_SubItem_Title">
                    Mô tả bài viết :
                  </div>
                  <div className="CreatePost_Container_Content_Item_SubItem_Input">
                    <textarea
                      onKeyDown={handleKeyDown}
                      placeholder="Nhập..."
                      value={c_description}
                      onChange={(e) => c_setDescription(e.target.value)}
                    ></textarea>
                  </div>
                </div>
              </div>
              <div className="CreatePost_Container_Content_Item">
                <div className="CreatePost_Container_Content_Item_SubItem">
                  <div className="CreatePost_Container_Content_Item_SubItem_Title">
                    Ảnh bìa bài viết :
                  </div>
                  <div className="CreatePost_Container_Content_Item_SubItem_Input">
                    {imageSrc !== "" ? (
                      <>
                        <div className="CreatePost_Container_Content_Item_SubItem_Input_Image">
                          <img src={imageSrc} alt="" />
                        </div>
                        <button onClick={handleDeleteImage}>Xóa ảnh</button>
                      </>
                    ) : (
                      <div className="CreatePost_Container_Content_Item_SubItem_Input_Upload">
                        <div>
                          <RiUploadCloud2Line />
                        </div>
                        <div>Drag in or upload from computer</div>
                        <div>
                          <input
                            type="file"
                            accept="image/*"
                            onChange={(e) => handleOnChangeImage(e)}
                          />
                          <label htmlFor="file-upload">Choose file</label>
                        </div>
                      </div>
                    )}
                  </div>
                </div>
                <div className="CreatePost_Container_Content_Item_SubItem">
                  <div className="CreatePost_Container_Content_Item_SubItem_Title">
                    Chọn danh mục :
                  </div>
                  <div className="CreatePost_Container_Content_Item_SubItem_Input">
                    <div className="CreatePost_Container_Content_Item_SubItem_Input_Choose">
                      {categories ? (
                        categories.map((item, index) => (
                          <div
                            key={index}
                            onClick={() => handleClick(index, item)}
                            className={
                              activeIndexes.includes(index) ? "Active" : ""
                            }
                          >
                            {item.type}
                          </div>
                        ))
                      ) : (
                        <>Không có danh mục nào ở đây!!</>
                      )}
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="CreatePost_Container_Btn">
              <button onClick={() => setIsOpen(true)}>
                Xem trước bài đăng <FiEye />
              </button>
              <button onClick={() => setIsLayoutReady(true)}>
                Tiếp theo <FiArrowRight />
              </button>
            </div>
          </div>
        ) : (
          <></>
        )}

        {isLayoutReady ? (
          <div className="CreatePost_CKEditor">
            <div
              className="CreatePost_CKEditor_Header_Back"
              onClick={() => setIsLayoutReady(false)}
            >
              <FiArrowLeft /> <span>Quay lại</span>
            </div>
            <h2 className="CreatePost_CKEditor_Header_Heading">
              Nhập nội dung bài viết:
            </h2>
            <div
              className="editor-container editor-container_classic-editor editor-container_include-style"
              ref={editorContainerRef}
            >
              <div className="editor-container__editor">
                <div ref={editorRef}>
                  <CKEditor
                    editor={ClassicEditor}
                    config={editorConfig}
                    data={editorData}
                    onReady={(editor) => {
                      editorInstanceRef.current = editor;
                    }}
                    onChange={(event, editor) => {
                      const data = editor.getData();
                      setEditorData(data);
                    }}
                  />
                </div>
                <div>
                  <button className="Button" onClick={handleSavePost}>
                    Lưu bài viết
                  </button>
                </div>
              </div>
            </div>
          </div>
        ) : (
          <></>
        )}
      </div>
    </>
  );
};

export default CreatePost;
