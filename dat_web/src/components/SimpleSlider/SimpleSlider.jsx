import "./SimpleSlider.scss";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { useContext } from "react";
import { DataContext } from "../Context/DataContext";
import {
  extractAfterDateTimeSuffix,
  extractBeforeDateTimeSuffix,
} from "../Format/Format";
import { useNavigate } from "react-router-dom";

const SimpleSlider = ({ data }) => {
  const navigate = useNavigate("");
  const { URL } = useContext(DataContext);
  const settings = {
    dots: true,
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 3000,
  };
  const handleNavigate = (item) => {
    navigate(`${item}`);
  };
  return (
    <>
      <div className="SimpleSlider">
        <div className="SimpleSlider_Container">
          <Slider {...settings}>
            {data.map((slide, index) => (
              <div key={index} className="Slider_Item">
                <div className="Slider_Item_Content">
                  <div className="Slider_Item_Content_Text">
                    <h2 className="Slider_Item_Content_Text_Title">
                      {extractBeforeDateTimeSuffix(slide.title)}
                    </h2>
                    <p className="Slider_Item_Content_Text_Desc">
                      {slide.content}
                    </p>
                    <div className="Slider_Item_Content_Text_Btn">
                      <button
                        onClick={() => handleNavigate(slide.highlight_id)}
                      >
                        Tìm hiểu thêm
                      </button>
                    </div>
                  </div>
                  <div className="Slider_Item_Content_Image">
                    <img
                      src={`${URL}/images/${extractAfterDateTimeSuffix(
                        slide.title
                      )}/${slide.image_url}`}
                      alt={`Slide ${index + 1}`}
                    />
                  </div>
                </div>
              </div>
            ))}
          </Slider>
        </div>
      </div>
    </>
  );
};

export default SimpleSlider;
