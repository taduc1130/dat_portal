import React, { useState, useRef, useContext } from "react";
import "./Setting.scss";
import { FiArrowLeft, FiEye, FiEyeOff } from "react-icons/fi";
import { callAPi } from "../../services/UserService";
import { AuthContext } from "../Context/AuthContext";
import { useNavigate } from "react-router-dom";

const Setting = () => {
  const navigate = useNavigate();
  const { URL, user, authDispatch } = useContext(AuthContext);

  const [oldPasswordIconState, setOldPasswordIconState] = useState("password");
  const [newPasswordIconState, setNewPasswordIconState] = useState("password");
  const [confirmPasswordIconState, setConfirmPasswordIconState] =
    useState("password");
  const oldPasswordRef = useRef(null);
  const newPasswordRef = useRef(null);
  const confirmPasswordRef = useRef(null);
  const validateInput = (id) => {
    if (id === "oldpassword") {
      const oldpasswordDom = document.getElementById(id);
      if (oldPasswordRef.current.value !== "") {
        oldpasswordDom.style.outline = "1px solid black";
        return true;
      } else {
        oldpasswordDom.style.outline = "2px solid red";
        return false;
      }
    } else if (id === "newpassword") {
      const newpasswordDom = document.getElementById(id);
      if (newPasswordRef.current.value !== "") {
        newpasswordDom.style.outline = "1px solid black";
        return true;
      } else {
        newpasswordDom.style.outline = "2px solid red";
        return false;
      }
    } else if (id === "confirmpassword") {
      const confirmpasswordDom = document.getElementById(id);
      if (confirmPasswordRef.current.value !== "") {
        if (confirmPasswordRef.current.value === newPasswordRef.current.value) {
          confirmpasswordDom.style.outline = "1px solid black";
          return true;
        } else {
          confirmpasswordDom.style.outline = "2px solid red";
          return false;
        }
      } else {
        confirmpasswordDom.style.outline = "2px solid red";
        return false;
      }
    }
  };
  const handleChangePassword = async () => {
    const oldpassword = validateInput("oldpassword");
    const newpassword = validateInput("newpassword");
    const confirmpassword = validateInput("confirmpassword");
    if (oldpassword && newpassword && confirmpassword) {
      let body = [
        user,
        oldPasswordRef.current.value,
        newPasswordRef.current.value,
      ];
      let res = await callAPi(
        "post",
        `${URL}/auth/admin-change-password`,
        body
      );
      if (!res.status) {
        alert(res.message);
      } else {
        alert(res.message);
        oldPasswordRef.current.value = "";
        newPasswordRef.current.value = "";
        confirmPasswordRef.current.value = "";
      }
    }
  };
  const handleLogout = async () => {
    authDispatch({
      type: "SET_LOGIN",
      payload: { state: false },
    });
    sessionStorage.removeItem("token");
    localStorage.removeItem("token");
    navigate("/");
  };
  return (
    <div className="Dat_Setting">
      <div className="Dat_Setting_Body">
        <h1>Change password</h1>
        <div className="Dat_Setting_Body_Changepassword">
          <div className="Dat_Setting_Body_Changepassword_Title">
            Old password:
          </div>
          <div
            id="oldpassword"
            className="Dat_Setting_Body_Changepassword_Inputcontainer"
          >
            <input
              className="Input"
              type={oldPasswordIconState}
              ref={oldPasswordRef}
              placeholder="old password"
            />
            {oldPasswordIconState === "password" ? (
              <FiEye
                className="Icon"
                onClick={() => setOldPasswordIconState("text")}
              />
            ) : (
              <FiEyeOff
                className="Icon"
                onClick={() => setOldPasswordIconState("password")}
              />
            )}
          </div>
        </div>
        <div className="Dat_Setting_Body_Changepassword">
          <div className="Dat_Setting_Body_Changepassword_Title">
            New password:
          </div>
          <div
            id="newpassword"
            className="Dat_Setting_Body_Changepassword_Inputcontainer"
          >
            <input
              className="Input"
              type={newPasswordIconState}
              ref={newPasswordRef}
              placeholder="new password"
            />
            {newPasswordIconState === "password" ? (
              <FiEye
                className="Icon"
                onClick={() => setNewPasswordIconState("text")}
              />
            ) : (
              <FiEyeOff
                className="Icon"
                onClick={() => setNewPasswordIconState("password")}
              />
            )}
          </div>
        </div>
        <div className="Dat_Setting_Body_Changepassword">
          <div className="Dat_Setting_Body_Changepassword_Title">
            Confirm password:
          </div>

          <div
            id="confirmpassword"
            className="Dat_Setting_Body_Changepassword_Inputcontainer"
          >
            <input
              className="Input"
              type={confirmPasswordIconState}
              ref={confirmPasswordRef}
              placeholder="confirm password"
            />
            {confirmPasswordIconState === "password" ? (
              <FiEye
                className="Icon"
                onClick={() => setConfirmPasswordIconState("text")}
              />
            ) : (
              <FiEyeOff
                className="Icon"
                onClick={() => setConfirmPasswordIconState("password")}
              />
            )}
          </div>
        </div>
        <div className="Dat_Setting_Body_Button">
          <button onClick={() => handleChangePassword()}>Update</button>
        </div>
      </div>
      <div className="Dat_Setting_Logout">
        <button
          onClick={() => {
            handleLogout();
          }}
        >
          Log out
        </button>
      </div>
    </div>
  );
};

export default Setting;
