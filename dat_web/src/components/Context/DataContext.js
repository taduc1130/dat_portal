import { createContext, useReducer } from "react";
import DataReducer, { INITIAL_STATE } from "../Context/DataReducer";
export const DataContext = createContext(INITIAL_STATE);

export const DataContextProvider = ({ children }) => {
    const [state, dataDispatch] = useReducer(DataReducer, INITIAL_STATE);
    return (
        <DataContext.Provider
            value={{
                URL: state.URL,
                headerMenu: state.headerMenu,
                categories: state.categories,
                highlightAll: state.highlightAll,
                highlightCategories: state.highlightCategories,
                newsNullCategory: state.newsNullCategory,
                newsHighLights: state.newsHighLights,
                newsCategories: state.newsCategories,
                dataDispatch,
            }}
        >
            {children}
        </DataContext.Provider>
    );
};
