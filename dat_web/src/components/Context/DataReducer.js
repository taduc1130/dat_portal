const INITIAL_STATE = {
  URL: "http://172.31.8.230:3001",
  categories: [],
  headerMenu: [],
  highlightAll: [],
  highlightCategories: [],
  newsNullCategory: [],
  newsHighLights: [],
  newsCategories: []
};
const DataReducer = (state, action) => {
  switch (action.type) {
    case "URL":
      return {
        ...state,
        URL: action.payload,
      }
    case "SET_HEADER_MENU":
      return {
        ...state,
        headerMenu: action.payload,
      }
    case "SET_CATEGORIES":
      return {
        ...state,
        categories: action.payload,
      }
    case "SET_HIGHLIGHTS_BY_ALL":
      return {
        ...state,
        highlightAll: action.payload
      }
    case "SET_HIGHLIGHTS_BY_CATEGORIES":
      return {
        ...state,
        highlightCategories: action.payload
      }
    case "SET_NEWS_NULL_CATEGORIES":
      return {
        ...state,
        newsNullCategory: action.payload
      }
    case "SET_NEWS_HIGHLIGHTS":
      return {
        ...state,
        newsHighLights: action.payload
      }
    case "SET_NEWS_CATEGORIES":
      return {
        ...state,
        newsCategories: action.payload
      }
    default:
      return state;
  }
};
export { INITIAL_STATE };
export default DataReducer;
