import { useState } from "react";
import "./Main.scss";
import { BrowserView, MobileView } from "react-device-detect";

const Main = () => {
  const [item, setItem] = useState(2);
  const handleChangeHref = (url) => {
    const newWindow = window.open(url, "_blank", "noopener,noreferrer");
    if (newWindow) newWindow.opener = null;
  };
  return (
    <>
      <BrowserView className="Main">
          <div className="Main_Container">
            <ul className="Main_Container_List">
              <li
                onClick={() => setItem(1)}
                className={`Main_Container_List_Item Elevator ${
                  item === 1 ? `` : `Hidden`
                }`}
              >
                <div className="Main_Container_List_Item_Fade"></div>
                <div className="Main_Container_List_Item_Left">
                  <div className="Main_Container_List_Item_Left_Heading">
                    AIoT Elev Website
                  </div>
                  <div className="Main_Container_List_Item_Left_Image">
                    <img src="/icons/elev.png" alt="" />
                  </div>
                  <div className="Main_Container_List_Item_Left_Button">
                    <button>Go to site</button>
                  </div>
                </div>
                <div className="Main_Container_List_Item_Right">
                  <div className="Main_Container_List_Item_Right_Heading">
                    <div className="Main_Container_List_Item_Right_Heading_Title">
                      Download AIoT Elevator
                    </div>
                    <div className="Main_Container_List_Item_Right_Heading_SubTitle">
                      Use your phone camera to sc an the QR code below
                    </div>
                  </div>
                  <div className="Main_Container_List_Item_Right_QR">
                    <div className="Main_Container_List_Item_Right_QR_Type">
                      <img
                        src="/icons/coming_soon.png"
                        alt=""
                        className="Main_Container_List_Item_Right_QR_Type_Img"
                      />
                      <div className="Main_Container_List_Item_Right_QR_Type_Download">
                        <div className="Main_Container_List_Item_Right_QR_Type_Download_Item">
                          <div className="Main_Container_List_Item_Right_QR_Type_Download_Item_Logo">
                            <img src="./icons/logo_apple.png" alt="" />
                          </div>
                          <div className="Main_Container_List_Item_Right_QR_Type_Download_Item_Content">
                            <div>Available on the</div>
                            <div>App Store</div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="Main_Container_List_Item_Right_QR_Type">
                      <img
                        src="/icons/coming_soon.png"
                        alt=""
                        className="Main_Container_List_Item_Right_QR_Type_Img"
                      />
                      <div className="Main_Container_List_Item_Right_QR_Type_Download">
                        <div className="Main_Container_List_Item_Right_QR_Type_Download_Item">
                          <div className="Main_Container_List_Item_Right_QR_Type_Download_Item_Logo">
                            <img src="./icons/chplay.png" alt="" />
                          </div>
                          <div className="Main_Container_List_Item_Right_QR_Type_Download_Item_Content">
                            <div>get it in</div>
                            <div>google play</div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </li>
              <li
                onClick={() => setItem(2)}
                className={`Main_Container_List_Item Energy ${
                  item === 2 ? `` : `Hidden`
                }`}
              >
                <div className="Main_Container_List_Item_Fade"></div>
                <div className="Main_Container_List_Item_Left">
                  <div className="Main_Container_List_Item_Left_Heading">
                    AIoT Energy Website
                  </div>
                  <div className="Main_Container_List_Item_Left_Image">
                    <img src="/icons/energy.png" alt="" />
                  </div>
                  <div className="Main_Container_List_Item_Left_Button">
                    <button>Go to site</button>
                  </div>
                </div>
                <div className="Main_Container_List_Item_Right">
                  <div className="Main_Container_List_Item_Right_Heading">
                    <div className="Main_Container_List_Item_Right_Heading_Title">
                      Download AIoT Energy
                    </div>
                    <div className="Main_Container_List_Item_Right_Heading_SubTitle">
                      Use your phone camera to sc an the QR code below
                    </div>
                  </div>
                  <div className="Main_Container_List_Item_Right_QR">
                    <div className="Main_Container_List_Item_Right_QR_Type">
                      <img
                        src="/images/qr_appstore.png"
                        alt=""
                        className="Main_Container_List_Item_Right_QR_Type_Img"
                      />
                      <div
                        onClick={() =>
                          handleChangeHref(
                            "https://apps.apple.com/vn/app/aiot-energy/id6484594898"
                          )
                        }
                        className="Main_Container_List_Item_Right_QR_Type_Download"
                      >
                        <div className="Main_Container_List_Item_Right_QR_Type_Download_Item">
                          <div className="Main_Container_List_Item_Right_QR_Type_Download_Item_Logo">
                            <img src="./icons/logo_apple.png" alt="" />
                          </div>
                          <div className="Main_Container_List_Item_Right_QR_Type_Download_Item_Content">
                            <div>Available on the</div>
                            <div>App Store</div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="Main_Container_List_Item_Right_QR_Type">
                      <img
                        src="/images/qr_chplay.png"
                        alt=""
                        className="Main_Container_List_Item_Right_QR_Type_Img"
                      />
                      <div
                        onClick={() =>
                          handleChangeHref(
                            "https://play.google.com/store/apps/details?id=vn.com.embody.aiot_energy&hl=vi"
                          )
                        }
                        className="Main_Container_List_Item_Right_QR_Type_Download"
                      >
                        <div className="Main_Container_List_Item_Right_QR_Type_Download_Item">
                          <div className="Main_Container_List_Item_Right_QR_Type_Download_Item_Logo">
                            <img src="./icons/chplay.png" alt="" />
                          </div>
                          <div className="Main_Container_List_Item_Right_QR_Type_Download_Item_Content">
                            <div>get it in</div>
                            <div>google play</div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </li>
              <li
                onClick={() => setItem(3)}
                className={`Main_Container_List_Item Automation ${
                  item === 3 ? `` : `Hidden`
                }`}
              >
                <div className="Main_Container_List_Item_Fade"></div>
                <div className="Main_Container_List_Item_Left">
                  <div className="Main_Container_List_Item_Left_Heading">
                    AIoT Automation Website
                  </div>
                  <div className="Main_Container_List_Item_Left_Image">
                    <img src="/icons/auto.png" alt="" />
                  </div>
                  <div className="Main_Container_List_Item_Left_Button">
                    <button>Go to site</button>
                  </div>
                </div>
                <div className="Main_Container_List_Item_Right">
                  <div className="Main_Container_List_Item_Right_Heading">
                    <div className="Main_Container_List_Item_Right_Heading_Title">
                      Download AIoT Automation
                    </div>
                    <div className="Main_Container_List_Item_Right_Heading_SubTitle">
                      Use your phone camera to sc an the QR code below
                    </div>
                  </div>
                  <div className="Main_Container_List_Item_Right_QR">
                    <div className="Main_Container_List_Item_Right_QR_Type">
                      <img
                        src="/icons/coming_soon.png"
                        alt=""
                        className="Main_Container_List_Item_Right_QR_Type_Img"
                      />
                      <div className="Main_Container_List_Item_Right_QR_Type_Download">
                        <div className="Main_Container_List_Item_Right_QR_Type_Download_Item">
                          <div className="Main_Container_List_Item_Right_QR_Type_Download_Item_Logo">
                            <img src="./icons/logo_apple.png" alt="" />
                          </div>
                          <div className="Main_Container_List_Item_Right_QR_Type_Download_Item_Content">
                            <div>Available on the</div>
                            <div>App Store</div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="Main_Container_List_Item_Right_QR_Type">
                      <img
                        src="/icons/coming_soon.png"
                        alt=""
                        className="Main_Container_List_Item_Right_QR_Type_Img"
                      />
                      <div className="Main_Container_List_Item_Right_QR_Type_Download">
                        <div className="Main_Container_List_Item_Right_QR_Type_Download_Item">
                          <div className="Main_Container_List_Item_Right_QR_Type_Download_Item_Logo">
                            <img src="./icons/chplay.png" alt="" />
                          </div>
                          <div className="Main_Container_List_Item_Right_QR_Type_Download_Item_Content">
                            <div>get it in</div>
                            <div>google play</div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </li>
            </ul>
          
        </div>
      </BrowserView>
      <MobileView className="Mobile_Main">
          <div className="Mobile_Main_Container">
            <ul className="Mobile_Main_Container_List">
              <li
                onClick={() => setItem(1)}
                className={`Mobile_Main_Container_List_Item Elevator ${
                  item === 1 ? `` : `Hidden`
                }`}
              >
                <div className="Mobile_Main_Container_List_Item_Fade"></div>
                <div className="Mobile_Main_Container_List_Item_Left">
                  <div className="Mobile_Main_Container_List_Item_Left_Heading">
                    AIoT Elev Website
                  </div>
                  <div className="Mobile_Main_Container_List_Item_Left_Image">
                    <img src="/icons/elev.png" alt="" />
                  </div>
                  <div className="Mobile_Main_Container_List_Item_Left_Button">
                    <button>Go to site</button>
                  </div>
                </div>
                <div className="Mobile_Main_Container_List_Item_Right">
                  <div className="Mobile_Main_Container_List_Item_Right_Heading">
                    <div className="Mobile_Main_Container_List_Item_Right_Heading_Title">
                      Download AIoT Elevator
                    </div>
                    <div className="Mobile_Main_Container_List_Item_Right_Heading_SubTitle">
                      Use your phone camera to scan the QR code below
                    </div>
                  </div>
                  <div className="Mobile_Main_Container_List_Item_Right_QR">
                    <div className="Mobile_Main_Container_List_Item_Right_QR_Type">
                      <img
                        src="/icons/coming_soon.png"
                        alt=""
                        className="Mobile_Main_Container_List_Item_Right_QR_Type_Img"
                      />
                      <div className="Mobile_Main_Container_List_Item_Right_QR_Type_Download">
                        <div className="Mobile_Main_Container_List_Item_Right_QR_Type_Download_Item">
                          <div className="Mobile_Main_Container_List_Item_Right_QR_Type_Download_Item_Logo">
                            <img src="./icons/logo_apple.png" alt="" />
                          </div>
                          <div className="Mobile_Main_Container_List_Item_Right_QR_Type_Download_Item_Content">
                            <div>Available on the</div>
                            <div>App Store</div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="Mobile_Main_Container_List_Item_Right_QR_Type">
                      <img
                        src="/icons/coming_soon.png"
                        alt=""
                        className="Mobile_Main_Container_List_Item_Right_QR_Type_Img"
                      />
                      <div className="Mobile_Main_Container_List_Item_Right_QR_Type_Download">
                        <div className="Mobile_Main_Container_List_Item_Right_QR_Type_Download_Item">
                          <div className="Mobile_Main_Container_List_Item_Right_QR_Type_Download_Item_Logo">
                            <img src="./icons/chplay.png" alt="" />
                          </div>
                          <div className="Mobile_Main_Container_List_Item_Right_QR_Type_Download_Item_Content">
                            <div>get it in</div>
                            <div>google play</div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </li>
              <li
                onClick={() => setItem(2)}
                className={`Mobile_Main_Container_List_Item Energy ${
                  item === 2 ? `` : `Hidden`
                }`}
              >
                <div className="Mobile_Main_Container_List_Item_Fade"></div>
                <div className="Mobile_Main_Container_List_Item_Left">
                  <div className="Mobile_Main_Container_List_Item_Left_Heading">
                    AIoT Energy Website
                  </div>
                  <div className="Mobile_Main_Container_List_Item_Left_Image">
                    <img src="/icons/energy.png" alt="" />
                  </div>
                  <div className="Mobile_Main_Container_List_Item_Left_Button">
                    <button>Go to site</button>
                  </div>
                </div>
                <div className="Mobile_Main_Container_List_Item_Right">
                  <div className="Mobile_Main_Container_List_Item_Right_Heading">
                    <div className="Mobile_Main_Container_List_Item_Right_Heading_Title">
                      Download AIoT Energy
                    </div>
                    <div className="Mobile_Main_Container_List_Item_Right_Heading_SubTitle">
                      Use your phone camera to scan the QR code below
                    </div>
                  </div>
                  <div className="Mobile_Main_Container_List_Item_Right_QR">
                    <div className="Mobile_Main_Container_List_Item_Right_QR_Type">
                      <img
                        src="/images/qr_appstore.png"
                        alt=""
                        className="Mobile_Main_Container_List_Item_Right_QR_Type_Img"
                      />
                      <div
                        onClick={() =>
                          handleChangeHref(
                            "https://apps.apple.com/vn/app/aiot-energy/id6484594898"
                          )
                        }
                        className="Mobile_Main_Container_List_Item_Right_QR_Type_Download"
                      >
                        <div className="Mobile_Main_Container_List_Item_Right_QR_Type_Download_Item">
                          <div className="Mobile_Main_Container_List_Item_Right_QR_Type_Download_Item_Logo">
                            <img src="./icons/logo_apple.png" alt="" />
                          </div>
                          <div className="Mobile_Main_Container_List_Item_Right_QR_Type_Download_Item_Content">
                            <div>Available on the</div>
                            <div>App Store</div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="Mobile_Main_Container_List_Item_Right_QR_Type">
                      <img
                        src="/images/qr_chplay.png"
                        alt=""
                        className="Mobile_Main_Container_List_Item_Right_QR_Type_Img"
                      />
                      <div
                        onClick={() =>
                          handleChangeHref(
                            "https://play.google.com/store/apps/details?id=vn.com.embody.aiot_energy&hl=vi"
                          )
                        }
                        className="Mobile_Main_Container_List_Item_Right_QR_Type_Download"
                      >
                        <div className="Mobile_Main_Container_List_Item_Right_QR_Type_Download_Item">
                          <div className="Mobile_Main_Container_List_Item_Right_QR_Type_Download_Item_Logo">
                            <img src="./icons/chplay.png" alt="" />
                          </div>
                          <div className="Mobile_Main_Container_List_Item_Right_QR_Type_Download_Item_Content">
                            <div>get it in</div>
                            <div>google play</div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </li>
              <li
                onClick={() => setItem(3)}
                className={`Mobile_Main_Container_List_Item Automation ${
                  item === 3 ? `` : `Hidden`
                }`}
              >
                <div className="Mobile_Main_Container_List_Item_Fade"></div>
                <div className="Mobile_Main_Container_List_Item_Left">
                  <div className="Mobile_Main_Container_List_Item_Left_Heading">
                    AIoT Automation Website
                  </div>
                  <div className="Mobile_Main_Container_List_Item_Left_Image">
                    <img src="/icons/auto.png" alt="" />
                  </div>
                  <div className="Mobile_Main_Container_List_Item_Left_Button">
                    <button>Go to site</button>
                  </div>
                </div>
                <div className="Mobile_Main_Container_List_Item_Right">
                  <div className="Mobile_Main_Container_List_Item_Right_Heading">
                    <div className="Mobile_Main_Container_List_Item_Right_Heading_Title">
                      Download AIoT Automation
                    </div>
                    <div className="Mobile_Main_Container_List_Item_Right_Heading_SubTitle">
                      Use your phone camera to scan the QR code below
                    </div>
                  </div>
                  <div className="Mobile_Main_Container_List_Item_Right_QR">
                    <div className="Mobile_Main_Container_List_Item_Right_QR_Type">
                      <img
                        src="/icons/coming_soon.png"
                        alt=""
                        className="Mobile_Main_Container_List_Item_Right_QR_Type_Img"
                      />
                      <div className="Mobile_Main_Container_List_Item_Right_QR_Type_Download">
                        <div className="Mobile_Main_Container_List_Item_Right_QR_Type_Download_Item">
                          <div className="Mobile_Main_Container_List_Item_Right_QR_Type_Download_Item_Logo">
                            <img src="./icons/logo_apple.png" alt="" />
                          </div>
                          <div className="Mobile_Main_Container_List_Item_Right_QR_Type_Download_Item_Content">
                            <div>Available on the</div>
                            <div>App Store</div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="Mobile_Main_Container_List_Item_Right_QR_Type">
                      <img
                        src="/icons/coming_soon.png"
                        alt=""
                        className="Mobile_Main_Container_List_Item_Right_QR_Type_Img"
                      />
                      <div className="Mobile_Main_Container_List_Item_Right_QR_Type_Download">
                        <div className="Mobile_Main_Container_List_Item_Right_QR_Type_Download_Item">
                          <div className="Mobile_Main_Container_List_Item_Right_QR_Type_Download_Item_Logo">
                            <img src="./icons/chplay.png" alt="" />
                          </div>
                          <div className="Mobile_Main_Container_List_Item_Right_QR_Type_Download_Item_Content">
                            <div>get it in</div>
                            <div>google play</div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </li>
            </ul>
          </div>
      </MobileView>
    </>
  );
};

export default Main;
