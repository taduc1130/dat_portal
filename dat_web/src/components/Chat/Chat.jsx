import "./Chat.scss";
import { FiSearch } from "react-icons/fi";
const Chat = () => {
  return (
    <>
      <div className="Chat">
        <div className="Chat_Container">
          <div className="Chat_Container_Sidebar">
            <div className="Chat_Container_Sidebar_Header">
              <div className="Chat_Container_Sidebar_Header_Search">
                <div className="Chat_Container_Sidebar_Header_Search_Icon">
                  <FiSearch />
                </div>
                <input type="text" placeholder="Search..." />
              </div>
            </div>
            <div className="Chat_Container_Sidebar_Conversations">
              <div className="Chat_Container_Sidebar_Conversations_Person"></div>
            </div>
          </div>
          <div className="Chat_Container_ChatArea"></div>
        </div>
      </div>
    </>
  );
};
export default Chat;
