import { FiSend } from "react-icons/fi";
import "./Footer.scss";
import { BrowserView, MobileView } from "react-device-detect";
const Footer = () => {
  return (
    <>
      <BrowserView className="Footer">
          <div className="Footer_Container">
            <div className="Footer_Container_Top">
              <div className="Footer_Container_Top_Item">
                <div className="Footer_Container_Top_Item_Heading">
                  KẾT NỐI VỚI
                  <div>
                    <img src="/icons/embody.png" alt="" />
                  </div>
                </div>
                <div className="Footer_Container_Top_Item_Icon">
                  <img src="/icons/elev.png" alt="" />
                  <img src="/icons/energy.png" alt="" />
                  <img src="/icons/auto.png" alt="" />
                </div>
              </div>
              <div className="Footer_Container_Top_Item">
                <div className="Footer_Container_Top_Item_Heading">
                  Danh mục
                </div>
                <ul className="Footer_Container_Top_Item_List">
                  <li className="Footer_Container_Top_Item_List_Item">
                    Energy
                  </li>
                  <li className="Footer_Container_Top_Item_List_Item">
                    Elevator
                  </li>
                  <li className="Footer_Container_Top_Item_List_Item">
                    Automation
                  </li>
                </ul>
              </div>
              <div className="Footer_Container_Top_Item">
                <div className="Footer_Container_Top_Item_Heading">Tin tức</div>
                <ul className="Footer_Container_Top_Item_List">
                  <li className="Footer_Container_Top_Item_List_Item">
                    Energy
                  </li>
                  <li className="Footer_Container_Top_Item_List_Item">
                    Elevator
                  </li>
                  <li className="Footer_Container_Top_Item_List_Item">
                    Automation
                  </li>
                </ul>
              </div>
              <div className="Footer_Container_Top_Item">
                <div className="Footer_Container_Top_Item_Heading">Liên hệ</div>
                <div className="Footer_Container_Top_Item_Text">
                  Đăng ký để nhận tin tức về sản phẩm và giải pháp mới nhất từ
                  Embody
                </div>
                <div className="Footer_Container_Top_Item_SendEmail">
                  <input type="text" placeholder="Email của bạn..." />
                  <div className="Footer_Container_Top_Item_SendEmail_Icon">
                    <FiSend />
                  </div>
                </div>
              </div>
            </div>
            <div className="Footer_Container_Devided"></div>
            <div className="Footer_Container_Bottom">
              <div className="Footer_Container_Bottom_Item">
                <div className="Footer_Container_Bottom_Item_Logo">
                  <img src="/icons/logo_embody.png" alt="" />
                </div>
                <div className="Footer_Container_Bottom_Item_Brand">
                  <img src="/icons/embody.png" alt="" />
                </div>
              </div>
              <div className="Footer_Container_Bottom_Item">
                <div className="Footer_Container_Bottom_Item_Copyright">
                  Copyright © 2020 - 2024 Embody.
                </div>
              </div>
            </div>
          </div>
      </BrowserView>
      <MobileView className="Mobile_Footer">
          <div className="Mobile_Footer_Container">
            <div className="Mobile_Footer_Container_Top">
              <div className="Mobile_Footer_Container_Top_Item">
                <div className="Mobile_Footer_Container_Top_Item_Heading">
                  KẾT NỐI VỚI
                  <div>
                    <img src="/icons/embody.png" alt="" />
                  </div>
                </div>
                <div className="Mobile_Footer_Container_Top_Item_Icon">
                  <img src="/icons/elev.png" alt="" />
                  <img src="/icons/energy.png" alt="" />
                  <img src="/icons/auto.png" alt="" />
                </div>
              </div>
              <div className="Mobile_Footer_Container_Top_Item">
                <div className="Mobile_Footer_Container_Top_Item_Heading">
                  Danh mục
                </div>
                <ul className="Mobile_Footer_Container_Top_Item_List">
                  <li className="Mobile_Footer_Container_Top_Item_List_Item">
                    Energy
                  </li>
                  <li className="Mobile_Mobile_Footer_Container_Top_Item_List_Item">
                    Elevator
                  </li>
                  <li className="Mobile_Footer_Container_Top_Item_List_Item">
                    Automation
                  </li>
                </ul>
              </div>
              <div className="Mobile_Footer_Container_Top_Item">
                <div className="Mobile_Footer_Container_Top_Item_Heading">Tin tức</div>
                <ul className="Mobile_Footer_Container_Top_Item_List">
                  <li className="Mobile_Footer_Container_Top_Item_List_Item">
                    Energy
                  </li>
                  <li className="Mobile_Footer_Container_Top_Item_List_Item">
                    Elevator
                  </li>
                  <li className="Mobile_Footer_Container_Top_Item_List_Item">
                    Automation
                  </li>
                </ul>
              </div>
              <div className="Mobile_Footer_Container_Top_Item">
                <div className="Mobile_Footer_Container_Top_Item_Heading">Liên hệ</div>
                <div className="Mobile_Footer_Container_Top_Item_Text">
                  Đăng ký để nhận tin tức về sản phẩm và giải pháp mới nhất từ
                  Embody
                </div>
                <div className="Mobile_Footer_Container_Top_Item_SendEmail">
                  <input type="text" placeholder="Email của bạn..." />
                  <div className="Mobile_Footer_Container_Top_Item_SendEmail_Icon">
                    <FiSend />
                  </div>
                </div>
              </div>
            </div>
            <div className="Mobile_Footer_Container_Devided"></div>
            <div className="Mobile_Footer_Container_Bottom">
              <div className="Mobile_Footer_Container_Bottom_Item">
                <div className="Mobile_Footer_Container_Bottom_Item_Logo">
                  <img src="/icons/logo_embody.png" alt="" />
                </div>
                <div className="Mobile_Footer_Container_Bottom_Item_Brand">
                  <img src="/icons/embody.png" alt="" />
                </div>
              </div>
              <div className="Mobile_Footer_Container_Bottom_Item">
                <div className="Mobile_Footer_Container_Bottom_Item_Copyright">
                  Copyright © 2020 - 2024 Embody.
                </div>
              </div>
            </div>
          </div>
      </MobileView>
    </>
  );
};

export default Footer;
