import "./Banner.scss";
import { BrowserView, MobileView } from "react-device-detect";
const Banner = () => {
  return (
    <>
      <BrowserView className="Banner">
          <div className="Banner_Container">
            <div className="Banner_Container_Text">
              <div className="Banner_Container_Text_Heading">
                Intelligent Monitoring System
              </div>
              <div className="Banner_Container_Text_Content">
                Comprehensive technology designed to provide convenience in
                industrial monitoring, device control and data analysis.
              </div>
            </div>
            <div className="Banner_Container_Image">
              <img src="/images/banner.png" alt="" />
            </div>
          </div>
      </BrowserView>
      <MobileView className="Mobile_Banner">
          <div className="Mobile_Banner_Container">
            <div className="Mobile_Banner_Container_Text">
              <div className="Mobile_Banner_Container_Text_Heading">
                Intelligent Monitoring System
              </div>
              
            </div>
            <div className="Mobile_Banner_Container_Image">
              <img src="/images/banner.png" alt="" />
            </div>
          </div>
      </MobileView>
    </>
  );
};

export default Banner;
