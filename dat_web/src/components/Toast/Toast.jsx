import React from "react";
import "./Toast.scss";
const Toast = ({ message }) => {
  return (
    <div className="Dat_Toast">
      <span>{message}</span>
    </div>
  );
};
export default Toast;
