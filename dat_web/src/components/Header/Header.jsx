import { useContext, useEffect, useState } from "react";
import "./Header.scss";
import { useNavigate } from "react-router-dom";
import { DataContext } from "../Context/DataContext";
import { FiChevronDown } from "react-icons/fi";
import { BrowserView, MobileView } from "react-device-detect";

const Header = () => {
  const navigate = useNavigate("");
  const { headerMenu } = useContext(DataContext);
  const [isScrolled, setIsScrolled] = useState(false);
  const [mainMenu, setMainMenu] = useState([]);
  const [isOpen, setIsOpen] = useState(false);
  const [subMenu, setSubMenu] = useState([]);

  const [mainMenuMobile, setMainMenuMobile] = useState([]);
  const [subMenuMobile, setSubMenuMobile] = useState([]);

  const handleScroll = () => {
    const scrollTop = window.scrollY;
    if (scrollTop > 500) {
      setIsScrolled(true);
    } else {
      setIsScrolled(false);
    }
  };
  const handleOpenMore = () => {
    setIsOpen(!isOpen);
  };
  useEffect(() => {
    setMainMenu(headerMenu.slice(0, 3));
    setSubMenu(headerMenu.slice(3));
    setMainMenuMobile(headerMenu.slice(0, 0));
    setSubMenuMobile(headerMenu.slice(0));
  }, [headerMenu]);



  useEffect(() => {
    window.addEventListener("scroll", handleScroll);
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, [isScrolled]);
  return (
    <>
    <BrowserView className={`Header ${isScrolled ? `Dark` : ``}`}>
        <div className="Header_Container">
          <div className="Header_Container_Brand" onClick={() => navigate("/")}>
            <div className="Header_Container_Brand_Logo">
              <img src="/icons/logo_embody.png" alt="" />
            </div>
            <div className="Header_Container_Brand_Name">
              <img src="/icons/embody.png" alt="" />
            </div>
          </div>
          <div className="Header_Container_Nav">
            <ul className="Header_Container_Nav_List">
              {mainMenu.map((item) => (
                <li key={item.index} className="Header_Container_Nav_List_Item">
                  <a href={item.link}>{item.name}</a>
                </li>
              ))}
              {subMenu.length > 0 && (
                <li className="Header_Container_Nav_List_Dropdown">
                  <div className="Header_Container_Nav_List_Dropdown_Item">
                    More
                    <span>
                      <FiChevronDown />
                    </span>
                  </div>
                  <ul className="Header_Container_Nav_List_Dropdown_Menu">
                    {subMenu.map((item) => (
                      <li
                        key={item.index}
                        className="Header_Container_Nav_List_Dropdown_Menu_Item"
                      >
                        <a href={item.link}>{item.name}</a>
                      </li>
                    ))}
                  </ul>
                </li>
              )}
            </ul>
          </div>
        </div>
    </BrowserView>
    <MobileView className={`Mobile_Header ${isScrolled ? `Dark` : ``}`}>
        <div className="Mobile_Header_Container">
          <div className="Mobile_Header_Container_Brand" onClick={() => navigate("/")}>
            <div className="Mobile_Header_Container_Brand_Logo">
              <img src="/icons/logo_embody.png" alt="" />
            </div>
            <div className="Mobile_Header_Container_Brand_Name">
              <img src="/icons/embody.png" alt="" />
            </div>
          </div>
          <div className="Mobile_Header_Container_Nav">
            <ul className="Mobile_Header_Container_Nav_List">
              {mainMenuMobile.map((item) => (
                <li key={item.index} className="Mobile_Header_Container_Nav_List_Item">
                  <a href={item.link}>{item.name}</a>
                </li>
              ))}
              {subMenuMobile.length > 0 && (
                <li className="Mobile_Header_Container_Nav_List_Dropdown">
                  <div className="Mobile_Header_Container_Nav_List_Dropdown_Item" onClick={() => handleOpenMore()}>
                    More
                    <span>
                      <FiChevronDown />
                    </span>
                  </div>
                  <ul className={`Mobile_Header_Container_Nav_List_Dropdown_Menu ${isOpen ? `Active` : ``}`}>
                    {subMenuMobile.map((item) => (
                      <li
                        key={item.index}
                        className="Mobile_Header_Container_Nav_List_Dropdown_Menu_Item"
                      >
                        <a href={item.link}>{item.name}</a>
                      </li>
                    ))}
                  </ul>
                </li>
              )}
            </ul>
          </div>
        </div>
    </MobileView>
      
    </>
  );
};

export default Header;
