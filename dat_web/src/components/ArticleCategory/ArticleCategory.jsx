import { useNavigate, useParams } from "react-router-dom";
import News from "../News/News";
import OutStandingPost from "../OutStandingPost/OutStandingPost";
import "./ArticleCategory.scss";
import { useContext, useEffect, useState } from "react";
import { AuthContext } from "../Context/AuthContext";
import { callAPi } from "../../services/UserService";
import { DataContext } from "../Context/DataContext";
import {
  extractAfterDateTimeSuffix,
  extractBeforeDateTimeSuffix,
} from "../Format/Format";

const ArticleCategory = () => {
  const navigate = useNavigate("");
  const { url } = useParams("");
  const { URL } = useContext(AuthContext);
  const { highlightAll } = useContext(DataContext);
  const [path, setPath] = useState("");
  const [data, setData] = useState([]);
  const [dataHighlight, setDataHighlight] = useState([]);
  const [dataNormal, setDataNormal] = useState([]);
  const [visibleCount, setVisibleCount] = useState(3);
  useEffect(() => {
    getArticleCategory();
  }, []);

  useEffect(() => {
    getCategories();
  }, [url]);

  const getCategories = async () => {
    let res = await callAPi("get", `${URL}/data/get-all-categories`);
    let data = res.data;
    if (!res.status) {
      return;
    }
    const urlIdNumber = parseInt(url, 10);
    const matchedItem = data.find((item) => item.id === urlIdNumber);
    if (matchedItem) {
      setPath(matchedItem.type);
    } else {
      setPath("");
    }
  };

  useEffect(() => {
    setDataHighlight(filterObjectsByIndex(data));
    setDataNormal(filterObjectsWithIndexZero(data));
  }, [data]);

  const filterObjectsByIndex = (data) => {
    return data.filter((obj) => obj.index !== 0);
  };

  function filterObjectsWithIndexZero(array) {
    return array.filter((obj) => obj.index === 0);
  }

  const getArticleCategory = async () => {
    let res = await callAPi("get", `${URL}/data/get-news-by-category/${url}`);
    if (!res.status) {
      console.log("error");
    }
    if (res.status) {
      let data = res.data;
      setData(data);
    }
  };

  const handleLoadMore = () => {
    setVisibleCount((prevCount) => prevCount + 3);
  };

  return (
    <>
      <div className="ArticleCategory">
        <div className="ArticleCategory_Container">
          <div className="ArticleCategory_Container_Location">
            <span onClick={() => navigate("/")}>Home</span> /{" "}
            <span>{path}</span>
          </div>
          <div className="ArticleCategory_Container_Heading">{path}</div>
          <div className="ArticleCategory_Container_Content">
            <div className="ArticleCategory_Container_Content_HighLight">
              {dataHighlight.length > 0 ? (
                <OutStandingPost
                  data={dataHighlight.filter((item) => item.index <= 3)}
                />
              ) : (
                <></>
              )}
            </div>
            <div className="ArticleCategory_Container_Content_News">
              <div className="ArticleCategory_Container_Content_News_Left">
                {dataHighlight
                  .filter((item) => item.index > 3)
                  .map((item, index) => (
                    <News
                      key={index}
                      id={item.id}
                      image={item.image_url}
                      title={item.title}
                      desc={item.content}
                      date={item.published_date}
                    />
                  ))}
                {dataNormal.slice(0, visibleCount).map((item, index) => (
                  <News
                    key={index}
                    id={item.id}
                    image={item.image_url}
                    title={item.title}
                    desc={item.content}
                    date={item.published_date}
                  />
                ))}
                <div className="ArticleCategory_Container_Content_News_Left_ViewMore">
                  {visibleCount < dataNormal.length && (
                    <button onClick={handleLoadMore}>Xem thêm</button>
                  )}
                </div>
              </div>
              <div className="ArticleCategory_Container_Content_News_Right">
                <div className="ArticleCategory_Container_Content_News_Right_Header">
                  Tin nổi bật
                </div>
                <div className="ArticleCategory_Container_Content_News_Right_Body">
                  <ul className="ArticleCategory_Container_Content_News_Right_Body_List">
                    {highlightAll && highlightAll.length > 0 ? (
                      highlightAll.map((item, index) => (
                        <li
                          key={index}
                          className="ArticleCategory_Container_Content_News_Right_Body_List_Item"
                          onClick={() =>
                            navigate(`/article-detail/${item.highlight_id}`)
                          }
                        >
                          <div
                            style={{
                              backgroundImage: `url("${URL}/images/${extractAfterDateTimeSuffix(
                                item.title
                              )}/${item.image_url}")`,
                            }}
                            className="ArticleCategory_Container_Content_News_Right_Body_List_Item_Image"
                          ></div>
                          <div className="ArticleCategory_Container_Content_News_Right_Body_List_Item_Title">
                            {extractBeforeDateTimeSuffix(item.title)}
                          </div>
                        </li>
                      ))
                    ) : (
                      <></>
                    )}
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default ArticleCategory;
