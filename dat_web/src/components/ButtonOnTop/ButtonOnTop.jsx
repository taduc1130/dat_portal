import { useEffect, useState } from "react";
import "./ButtonOnTop.scss";
import { FiChevronUp } from "react-icons/fi";

const ButtonOnTop = () => {
  const [isVisible, setIsVisible] = useState(false);

  // Xử lý sự kiện scroll của trang
  const handleScroll = () => {
    const scrollTop = window.pageYOffset;
    if (scrollTop > 300) {
      // Khi cuộn xuống dưới 300px thì hiển thị nút
      setIsVisible(true);
    } else {
      setIsVisible(false);
    }
  };
  const scrollToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: "smooth", // Cuộn mượt đến đầu trang
    });
  };

  // Thêm sự kiện cho window khi component mount và unmount
  useEffect(() => {
    window.addEventListener("scroll", handleScroll);
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);
  return (
    <>
      <div className="ButtonOnTop" onClick={scrollToTop}>
        <div className={`ButtonOnTop_Icon ${isVisible ? "Show" : ""}`}>
          <FiChevronUp />
        </div>
      </div>
    </>
  );
};

export default ButtonOnTop;
