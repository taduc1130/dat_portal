import React, { useEffect, useRef } from 'react';
import Litepicker from 'litepicker';
import './Litepicker.css';
import 'litepicker/dist/plugins/ranges'; // Import plugin predefined ranges

const DatePicker = ({ onDateSelect }) => {
  const datepickerRef = useRef(null);

  useEffect(() => {
    if (datepickerRef.current) {
      const picker = new Litepicker({
        element: datepickerRef.current,
        singleMode: false, // Cho phép chọn khoảng thời gian
        numberOfMonths: 2, // Hiển thị hai bảng
        numberOfColumns: 2, // Hai bảng trên cùng một hàng
        autoApply: true, // Tự động áp dụng khi người dùng chọn ngày
        format: 'DD-MM-YYYY',
        plugins: ['ranges'], // Sử dụng plugin predefined ranges

        ranges: {
          'Last 7 Days': [
            new Date(new Date().setDate(new Date().getDate() - 7)),
            new Date()
          ],
          'Last 30 Days': [
            new Date(new Date().setDate(new Date().getDate() - 30)),
            new Date()
          ],
          'This Month': [
            new Date(new Date().getFullYear(), new Date().getMonth(), 1),
            new Date()
          ],
          'Last Month': [
            new Date(new Date().getFullYear(), new Date().getMonth() - 1, 1),
            new Date(new Date().getFullYear(), new Date().getMonth(), 0)
          ]
        },
        
        setup: (picker) => {
            picker.on('selected', (start, end) => {
              const startDate = start.format('YYYY-MM-DD');
              const endDate = end.format('YYYY-MM-DD');
              onDateSelect(startDate, endDate);
            });
          }
      });

      // Cleanup khi component unmount
      return () => picker.destroy();
    }
  }, []);

  return (
    <div>
      <input type="text" ref={datepickerRef}  style={{border:"none",outline: "none"}}  placeholder="Select a date range" />
    </div>
  );
};

export default DatePicker;
