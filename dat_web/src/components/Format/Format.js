export const removeAccents = (str) => {
    return str.normalize("NFD").replace(/[\u0300-\u036f]/g, "").replace(/đ/g, "d").replace(/Đ/g, "D");
};

export const getCurrentDateTime = () => {
    const now = new Date();
    const dd = String(now.getDate()).padStart(2, '0');
    const mm = String(now.getMonth() + 1).padStart(2, '0'); // Tháng bắt đầu từ 0
    const yyyy = now.getFullYear();
    const hh = String(now.getHours()).padStart(2, '0');
    const mi = String(now.getMinutes()).padStart(2, '0');
    const ss = String(now.getSeconds()).padStart(2, '0');

    return `${dd}${mm}${yyyy}_${hh}${mi}${ss}`;
}

export const formatStringWithDatTime = (input) => {
    const dateTimeSuffix = getCurrentDateTime();
    return `${input}__${dateTimeSuffix}`;
}

export const extractBeforeDateTimeSuffix = (str) => {
    const parts = str.split('__');
    if (parts.length < 2) {
        return str; // Không tìm thấy dấu __, trả về chuỗi gốc
    }
    return parts[0]; // Trả về phần chuỗi trước dấu __
}

export const extractAfterDateTimeSuffix = (str) => {
    const parts = str.split('__');
    if (parts.length < 2) {
        return ''; // Không tìm thấy dấu __, trả về chuỗi rỗng
    }
    return parts[1]; // Trả về phần chuỗi sau dấu __
}

export const capitalizeFirstWord = (str) => {
    const words = str.split(' ');
    if (words.length > 0) {
        words[0] = words[0].charAt(0).toUpperCase() + words[0].slice(1);
    }
    return words.join(' ');
}

export const getFirstFewWords = (str, wordCount) => {
    const words = str.split(' ');
    if (words.length <= wordCount) {
        return str;
    }
    const firstFewWords = words.slice(0, wordCount).join(' ');
    return `${firstFewWords} ...`;
}

export const formatDateToDDMMYYYY = (isoDateString) => {
    const dateObject = new Date(isoDateString);
    const year = dateObject.getFullYear();
    const month = String(dateObject.getMonth() + 1).padStart(2, "0");
    const day = String(dateObject.getDate()).padStart(2, "0");

    return `${day}/${month}/${year}`;
}