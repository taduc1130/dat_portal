import { useNavigate, useParams } from "react-router-dom";
import "./NewsDetail.scss";
import { useContext, useEffect, useState } from "react";
import { callAPi } from "../../services/UserService";
import {
  extractAfterDateTimeSuffix,
  extractBeforeDateTimeSuffix,
  getFirstFewWords,
} from "../Format/Format";
import { DataContext } from "../Context/DataContext";
const NewsDetail = () => {
  const navigate = useNavigate("");
  const { url } = useParams("");
  let { URL, highlightAll } = useContext(DataContext);
  const [title, setTitle] = useState("");
  const [htmlData, setHtmlData] = useState("");
  const [path, setPath] = useState("");

  useEffect(() => {
    getArticle();
  }, []);

  const getArticle = async () => {
    let res = await callAPi("get", `${URL}/data/get-news-by-id/${url}`);
    if (!res.status) {
      setHtmlData("Không có nội dung!");
      setPath("");
      setTitle("Không có nội dung!");
    }
    if (res.status) {
      if (res.data[0]) {
        let data = res.data[0];
        let extractTitle = extractBeforeDateTimeSuffix(data.title);
        let extractPath = getFirstFewWords(extractTitle, 4);
        setPath(extractPath);
        setTitle(extractTitle);
        setHtmlData(data.newbody);
      }
    }
  };

  return (
    <>
      <div className="NewsDetail">
        <div className="NewsDetail_Path">
          <div className="NewsDetail_Path_Content">
            <span onClick={() => navigate("/")}>Home</span> /{" "}
            <span>{path}</span>
          </div>
        </div>
        <div className="NewsDetail_Container">
          <div className="NewsDetail_Container_Body">
            <div className="NewsDetail_Container_Body_Title">{title}</div>
            <div
              className="NewsDetail_Container_Body_Content"
              dangerouslySetInnerHTML={{ __html: htmlData }}
            ></div>
            <div className="NewsDetail_Container_Body_Comment">
              <div className="NewsDetail_Container_Body_Comment_Title">
                Comment
              </div>
              <div className="NewsDetail_Container_Body_Comment_Input">
                <textarea placeholder="Bình luận"></textarea>
                <button>Gửi</button>
              </div>
            </div>
          </div>
          <div className="NewsDetail_Container_Additional">
            <div className="NewsDetail_Container_Additional_Header">
              Tin nổi bật
            </div>
            <div className="NewsDetail_Container_Additional_Body">
              <ul className="NewsDetail_Container_Additional_Body_List">
                {highlightAll && highlightAll.length > 0 ? (
                  highlightAll.map((item, index) => (
                    <li
                      key={index}
                      className="NewsDetail_Container_Additional_Body_List_Item"
                    >
                      <div
                        style={{
                          backgroundImage: `url("${URL}/images/${extractAfterDateTimeSuffix(
                            item.title
                          )}/${item.image_url}")`,
                        }}
                        className="NewsDetail_Container_Additional_Body_List_Item_Image"
                      ></div>
                      <div className="NewsDetail_Container_Additional_Body_List_Item_Title">
                        {extractBeforeDateTimeSuffix(item.title)}
                      </div>
                    </li>
                  ))
                ) : (
                  <></>
                )}
              </ul>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default NewsDetail;
