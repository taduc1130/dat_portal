import { useEffect, useState } from "react";
import "./Manage.scss";
import { menu } from "../../App";
import ManageNews from "./Child/ManageNews";
import ManageNavbar from "./Child/ManageNavbar";
import ManageCategory from "./Child/ManageCategory";

const Manage = () => {
  const [state, setState] = useState("news");
  useEffect(() => {
    setState(menu.value);
    if (menu.value === "navbar") {
      setState("navbar");
    }
    if (menu.value === "category") {
      setState("category");
    }
    if (menu.value === "news") {
      setState("news");
    }
  }, [menu.value]);
  return (
    <>
      <div className="Manage">
        {state && state === "news" ? (
          <ManageNews />
        ) : state === "category" ? (
          <ManageCategory />
        ) : (
          <ManageNavbar />
        )}
      </div>
    </>
  );
};

export default Manage;
