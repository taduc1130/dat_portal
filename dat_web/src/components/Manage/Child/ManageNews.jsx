import {
  FiChevronRight,
  FiEdit,
  FiPlusCircle,
  FiSearch,
  FiTrash2,
} from "react-icons/fi";
import DatePicker from "../../Litepicker/Litepicker";
import DataTable from "react-data-table-component";
import { useContext, useEffect, useState } from "react";
import { DataContext } from "../../Context/DataContext";
import dayjs from "dayjs";
import { Tooltip } from "react-tooltip";
import Form from "react-bootstrap/Form";
import { useNavigate } from "react-router-dom";

const ManageNews = () => {
  const navigate = useNavigate("");
  const [title, setTitle] = useState("");
  const [data, setData] = useState([]);
  const [column, setColumn] = useState([]);
  const { categories, newsHighLights, newsNullCategory, newsCategories } =
    useContext(DataContext);

  let CustomIconAction = ({ id }) => {
    return (
      <div style={{ gap: ".75rem", display: "flex" }}>
        <span
          className="IconDataTable Primary"
          style={{ padding: "2px 4px", cursor: "pointer" }}
          // onClick={() => handleEdit(id)}
        >
          <FiEdit size={16} />
        </span>
        <span
          className="IconDataTable Danger"
          style={{ padding: "2px 4px", cursor: "pointer" }}
          // onClick={() => handleDelete(id)}
        >
          <FiTrash2 size={16} />
        </span>
      </div>
    );
  };
  let CustomIconActionHighLights = ({ id }) => {
    return (
      <div style={{ gap: ".75rem", display: "flex" }}>
        <span
          className="IconDataTable Primary"
          style={{ padding: "2px 4px", cursor: "pointer" }}
          // onClick={() => handleEdit(id)}
        >
          <FiEdit size={16} />
        </span>
      </div>
    );
  };
  const columnNews = [
    {
      name: "ID",
      selector: (row) => row.id,
      width: "70px",
      sortable: true,
    },
    {
      name: "Title",
      selector: (row) => (
        <div data-tooltip-id="title" data-tooltip-content={row.title}>
          {row.title}
          <Tooltip
            id="title"
            effect="solid"
            place="top"
            type="dark"
            className="custom-tooltip"
          />
        </div>
      ),
      width: "200px",
    },
    {
      name: "Content",
      selector: (row) => (
        <div data-tooltip-id="content" data-tooltip-content={row.content}>
          {row.content}
          <Tooltip
            id="content"
            effect="solid"
            place="top"
            type="dark"
            className="custom-tooltip"
          />
        </div>
      ),
      width: "200px",
    },
    {
      name: "Image",
      selector: (row) => (
        <div data-tooltip-id="image_url" data-tooltip-content={row.image_url}>
          {row.image_url}
          <Tooltip
            id="image_url"
            effect="solid"
            place="top"
            type="dark"
            className="custom-tooltip"
          />
        </div>
      ),
      width: "150px",
    },
    {
      name: "Index",
      selector: (row) => (
        <div style={{ display: "flex", justifyContent: "center" }}>
          <input
            type="number"
            className="InputIndex"
            value={
              row.index !== null && row.index !== undefined ? row.index : ""
            }
            // onChange={(e) => {
            //   handleOnchangeIndex(e, row.id);
            // }}
            // onBlur={() => {
            //   handleEditIndex(row.i);
            // }}
            min={0}
          />
        </div>
      ),
      width: "95px",
    },
    {
      name: "Status",
      selector: (row) => (
        <div style={{ display: "flex", justifyContent: "center" }}>
          <Form.Check
            className="Check"
            checked={row.status}
            // onChange={(e) => {
            //   handleEditPublish(e, row.id);
            // }}
          />
        </div>
      ),
      width: "70px",
    },
    {
      name: "HighLight",
      selector: (row) => (
        <div style={{ display: "flex", justifyContent: "center" }}>
          <Form.Check
            className="Check"
            checked={row.is_highlighted}
            // onChange={(e) => {
            //   handleEditHighlight(e, row.id);
            // }}
          />
        </div>
      ),
      width: "90px",
    },
    {
      name: "Date Publish",
      selector: (row) => {
        if (row.published_date === undefined || row.published_date === null)
          return null;
        return dayjs(row.published_date.split("T")[0]).format("DD-MM-YYYY");
      },
      width: "150px",
    },
    {
      name: "Date Update",
      selector: (row) => {
        if (row.updated_date === undefined || row.updated_date === null)
          return null;

        return dayjs(row.updated_date.split("T")[0]).format("DD-MM-YYYY");
      },
      width: "150px",
    },
    {
      name: "Manage",
      selector: (row) => <CustomIconAction id={row.id} />,
    },
  ];
  const columnNewsHighlight = [
    {
      name: "ID",
      selector: (row) => row.id,
      width: "70px",
      sortable: true,
    },
    {
      name: "Categories",
      selector: (row) => {
        if (row.categories === undefined) return null;
        let categoriesString;
        if (row.categories === null) {
          categoriesString = "----";
        } else {
          categoriesString = row.categories
            .map((category) => category.category_name)
            .join(", ");
        }

        return (
          <div
            data-tooltip-id="categories"
            data-tooltip-content={categoriesString}
          >
            {categoriesString}
            <Tooltip
              id="categories"
              effect="solid"
              place="top"
              type="dark"
              className="custom-tooltip"
            />
          </div>
        );
      },
      width: "170px",
      sortable: true,
    },
    {
      name: "Title",
      selector: (row) => (
        <div data-tooltip-id="title" data-tooltip-content={row.title}>
          {row.title}
          <Tooltip
            id="title"
            effect="solid"
            place="top"
            type="dark"
            className="custom-tooltip"
          />
        </div>
      ),
      width: "200px",
    },
    {
      name: "Content",
      selector: (row) => (
        <div data-tooltip-id="content" data-tooltip-content={row.content}>
          {row.content}
          <Tooltip
            id="content"
            effect="solid"
            place="top"
            type="dark"
            className="custom-tooltip"
          />
        </div>
      ),
      width: "200px",
    },
    {
      name: "Image",
      selector: (row) => (
        <div data-tooltip-id="image_url" data-tooltip-content={row.image_url}>
          {row.image_url}
          <Tooltip
            id="image_url"
            effect="solid"
            place="top"
            type="dark"
            className="custom-tooltip"
          />
        </div>
      ),
      width: "150px",
    },
    {
      name: "Index",
      selector: (row) => (
        <div style={{ display: "flex", justifyContent: "center" }}>
          <input
            type="number"
            className="InputIndex"
            value={
              row.index !== null && row.index !== undefined ? row.index : ""
            }
            // onChange={(e) => {
            //   handleOnchangeIndex(e, row.id);
            // }}
            // onBlur={() => {
            //   handleEditIndex(row.i);
            // }}
            min={0}
          />
        </div>
      ),
      width: "95px",
    },
    {
      name: "Status",
      selector: (row) => (
        <div style={{ display: "flex", justifyContent: "center" }}>
          <Form.Check
            className="Check"
            checked={row.status}
            // onChange={(e) => {
            //   handleEditPublish(e, row.id);
            // }}
          />
        </div>
      ),
      width: "70px",
    },
    {
      name: "Date Publish",
      selector: (row) => {
        if (row.published_date === undefined || row.published_date === null)
          return null;
        return dayjs(row.published_date.split("T")[0]).format("DD-MM-YYYY");
      },
      width: "150px",
    },
    {
      name: "Date Update",
      selector: (row) => {
        if (row.updated_date === undefined || row.updated_date === null)
          return null;

        return dayjs(row.updated_date.split("T")[0]).format("DD-MM-YYYY");
      },
      width: "150px",
    },
    {
      name: "Manage",
      selector: (row) => <CustomIconActionHighLights id={row.id_news} />,
    },
  ];

  const handleSetData = (column, data, type) => {
    setColumn(column);
    setTitle(type);
    setData(data);
  };

  useEffect(() => {
    setColumn(columnNewsHighlight);
    setData(newsHighLights);
    setTitle("Highlights News");
  }, [newsHighLights, newsNullCategory]);

  return (
    <>
      <div className="ManageNews">
        <div className="ManageNews_Header">
          <div className="ManageNews_Header_Sort">
            <div className="ManageNews_Header_Sort_Item">
              <div className="ManageNews_Header_Sort_Item_Category">
                <div className="ManageNews_Header_Sort_Item_Category_Text">
                  {title}
                </div>
                <div className="ManageNews_Header_Sort_Item_Category_Icon">
                  <FiChevronRight />
                </div>
                <div className="ManageNews_Header_Sort_Item_Category_Dropdown">
                  <div
                    className="ManageNews_Header_Sort_Item_Category_Dropdown_Item"
                    onClick={() => {
                      handleSetData(
                        columnNewsHighlight,
                        newsHighLights,
                        "Highlights News"
                      );
                    }}
                  >
                    Highlights News
                  </div>
                  {categories.length > 0 &&
                    categories.map((item, index) => (
                      <div
                        className="ManageNews_Header_Sort_Item_Category_Dropdown_Item"
                        key={index}
                        onClick={() => {
                          handleSetData(
                            columnNews,
                            newsCategories[item.id],
                            item.type
                          );
                        }}
                      >
                        {item.type}
                      </div>
                    ))}
                  <div
                    className="ManageNews_Header_Sort_Item_Category_Dropdown_Item"
                    onClick={() => {
                      handleSetData(
                        columnNews,
                        newsNullCategory,
                        "Không có danh mục"
                      );
                    }}
                  >
                    Không có danh mục
                  </div>
                </div>
              </div>
            </div>
            <div className="ManageNews_Header_Sort_Item">
              <div className="ManageNews_Header_Sort_Item_Search">
                <div className="ManageNews_Header_Sort_Item_Search_Icon">
                  <FiSearch />
                </div>
                <div className="ManageNews_Header_Sort_Item_Content_Input">
                  <input type="text" placeholder="Tìm kiếm..." />
                </div>
              </div>
            </div>
            <div className="ManageNews_Header_Sort_Item">
              <div className="ManageNews_Header_Sort_Item_Date">
                <DatePicker />
              </div>
            </div>
          </div>
          <div className="ManageNews_Header_Create">
            <div
              className="ManageNews_Header_Create_Item"
              onClick={() => navigate("/admin/createarticle")}
            >
              <div className="ManageNews_Header_Create_Item_Text">Create</div>
              <div className="ManageNews_Header_Create_Item_Icon">
                <FiPlusCircle />
              </div>
            </div>
          </div>
        </div>
        <div className="ManageNews_Table">
          <DataTable
            title={title}
            columns={column}
            fixedHeader={true}
            data={data}
            fixedHeaderScrollHeight="400px"
            pagination
            highlightOnHover={true}
          />
        </div>
      </div>
    </>
  );
};

export default ManageNews;
