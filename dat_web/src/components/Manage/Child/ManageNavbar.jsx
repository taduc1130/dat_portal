import { useContext, useState } from "react";
import DataTable from "react-data-table-component";
import { FiEdit, FiPlusCircle, FiTrash2 } from "react-icons/fi";
import { DataContext } from "../../Context/DataContext";
import Form from "react-bootstrap/Form";
import { Tooltip } from "react-tooltip";
import { callAPi } from "../../../services/UserService";

const ManageNavbar = () => {
  const [isOpen, setIsOpen] = useState(false);
  const [isOpenChange, setIsOpenChange] = useState(false);
  const [data, setData] = useState([]);
  const { headerMenu, URL, dataDispatch } = useContext(DataContext);
  const handleClose = () => {
    setIsOpen(false);
  };
  const handleCloseChange = () => {
    setIsOpenChange(false);
  };
  const handleChange = (id, name, link) => {
    setIsOpenChange(true);
    setData({ id, name, link });
  };
  const handleDelete = async (id) => {
    let res = await callAPi("post", `${URL}/data/delete-menu`, [id]);
    if (!res.status) {
      alert(res.message);
      return;
    }
    if (res.status) {
      let temp = [...headerMenu];
      const i = temp.findIndex((item) => item.id === id);
      temp.splice(i, 1);
      dataDispatch({
        type: "SET_HEADER_MENU",
        payload: temp,
      });
      alert(res.message);
    }
  };
  const CustomIconAction = ({ id, name, link }) => (
    <div style={{ gap: ".75rem", display: "flex" }}>
      <span
        className="IconDataTable Primary"
        style={{ padding: "2px 4px", cursor: "pointer" }}
      >
        <FiEdit size={16} onClick={() => handleChange(id, name, link)} />
      </span>
      <span
        className="IconDataTable Danger"
        style={{ padding: "2px 4px", cursor: "pointer" }}
        onClick={() => handleDelete(id)}
      >
        <FiTrash2 size={16} />
      </span>
    </div>
  );
  const columnNavbar = [
    {
      name: "ID",
      selector: (row) => row.id,
      width: "70px",
      sortable: true,
    },
    {
      name: "Name",
      selector: (row) => (
        <div data-tooltip-id="name" data-tooltip-content={row.name}>
          {row.name}
          <Tooltip
            id="name"
            effect="solid"
            place="top"
            type="dark"
            className="custom-tooltip"
          />
        </div>
      ),
      width: "200px",
    },
    {
      name: "link",
      selector: (row) => (
        <div data-tooltip-id="link" data-tooltip-content={row.link}>
          {row.link}
          <Tooltip
            id="link"
            effect="solid"
            place="top"
            type="dark"
            className="custom-tooltip"
          />
        </div>
      ),
      width: "200px",
    },
    {
      name: "Index",
      selector: (row) => (
        <div style={{ display: "flex", justifyContent: "center" }}>
          <input
            type="number"
            className="InputIndex"
            value={
              row.index !== null && row.index !== undefined ? row.index : 0
            }
            min={0}
          />
        </div>
      ),
      width: "95px",
    },
    {
      name: "Status",
      selector: (row) => (
        <div style={{ display: "flex", justifyContent: "center" }}>
          <Form.Check className="Check" checked={row.status} />
        </div>
      ),
      width: "70px",
    },
    {
      name: "Manage",
      selector: (row) => (
        <CustomIconAction name={row.name} link={row.link} id={row.id} />
      ),
    },
  ];

  return (
    <>
      <div className="ManageNavbar">
        <div className="ManageNavbar_Header">
          <div className="ManageNavbar_Header_Item">
            <div
              className="ManageNavbar_Header_Item_Content"
              onClick={() => setIsOpen(true)}
            >
              <div className="ManageNavbar_Header_Item_Content_Text">
                Create
              </div>
              <div className="ManageNavbar_Header_Item_Content_Icon">
                <FiPlusCircle />
              </div>
            </div>
            {isOpen && <Pop handleClose={handleClose} />}
          </div>
        </div>
        <div className="ManageNavbar_Table">
          <DataTable
            title="Navigation Bar"
            columns={columnNavbar}
            data={headerMenu}
            fixedHeader={true}
            fixedHeaderScrollHeight="400px"
            pagination
            highlightOnHover={true}
          />
          {isOpenChange && (
            <PopChange handleClose={handleCloseChange} data={data} />
          )}
        </div>
      </div>
    </>
  );
};
const Pop = ({ handleClose }) => {
  const { URL, dataDispatch, headerMenu } = useContext(DataContext);
  const [name, setName] = useState("");
  const [link, setLink] = useState("");

  const handleChange = (setItem, data) => {
    setItem(data.target.value);
  };
  const handleSubmit = async () => {
    let body = [name, link];
    if (name === "" || link === "") {
      alert("Vui lòng nhập đầy đủ thông tin!");
      return;
    }
    let res = await callAPi("post", `${URL}/data/add-menu`, body);
    if (!res.status) {
      alert(res.message);
      return;
    }
    if (res.status) {
      alert(res.message);
      dataDispatch({
        type: "SET_HEADER_MENU",
        payload: [
          ...headerMenu,
          {
            id: res.id,
            name: name,
            link: link,
            index: 0,
            status: false,
          },
        ],
      });
      handleClose();
    }
  };
  return (
    <>
      <div className="Pop">
        <div className="Pop_Container">
          <div className="Pop_Container_Input">
            <input
              type="text"
              value={name}
              placeholder="Tên danh mục..."
              onChange={(e) => handleChange(setName, e)}
            />
          </div>
          <div className="Pop_Container_Input">
            <input
              type="text"
              value={link}
              placeholder="Đường dẫn..."
              onChange={(e) => handleChange(setLink, e)}
            />
          </div>
          <div className="Pop_Container_Button">
            <div
              className="Pop_Container_Button_Confirm"
              onClick={handleSubmit}
            >
              Xác nhận
            </div>
            <div className="Pop_Container_Button_Close" onClick={handleClose}>
              Hủy bỏ
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
const PopChange = ({ data, handleClose }) => {
  const { URL, dataDispatch, headerMenu } = useContext(DataContext);
  const [dataNavbar, setDataNavbar] = useState(data);
  const handleOnChange = (value, field) => {
    setDataNavbar((prevData) => ({
      ...prevData,
      [field]: value,
    }));
  };
  const handleSubmit = async () => {
    if (dataNavbar.name === "" || dataNavbar.link === "") {
      alert("Vui lòng nhập đầy đủ thông tin!");
      return;
    }
    let res = await callAPi("post", `${URL}/data/edit-name-menu`, [
      data.id,
      dataNavbar.name,
      dataNavbar.link,
    ]);
    if (!res.status) {
      alert(res.message);
      return;
    }
    if (res.status) {
      alert(res.message);
      const updatedMenu = headerMenu.map((item) =>
        item.id === data.id
          ? { ...item, name: dataNavbar.name, link: dataNavbar.link }
          : item
      );
      dataDispatch({
        type: "SET_HEADER_MENU",
        payload: updatedMenu,
      });
      handleClose();
    }
  };
  return (
    <>
      <div className="Pop">
        <div className="Pop_Container">
          <div className="Pop_Container_Input">
            <input
              type="text"
              value={dataNavbar.name}
              placeholder="Tên danh mục..."
              onChange={(e) => handleOnChange(e.target.value, "name")}
            />
          </div>
          <div className="Pop_Container_Input">
            <input
              type="text"
              value={dataNavbar.link}
              placeholder="Đường dẫn..."
              onChange={(e) => handleOnChange(e.target.value, "link")}
            />
          </div>
          <div className="Pop_Container_Button">
            <div
              className="Pop_Container_Button_Confirm"
              onClick={handleSubmit}
            >
              Xác nhận
            </div>
            <div className="Pop_Container_Button_Close" onClick={handleClose}>
              Hủy bỏ
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
export default ManageNavbar;
