import { useContext, useState } from "react";
import DataTable from "react-data-table-component";
import { FiEdit, FiPlusCircle, FiTrash2 } from "react-icons/fi";
import Form from "react-bootstrap/Form";
import { DataContext } from "../../Context/DataContext";
import { Tooltip } from "react-tooltip";
import { callAPi } from "../../../services/UserService";

const ManageCategory = () => {
  const [isOpen, setIsOpen] = useState(false);
  const [isOpenChange, setIsOpenChange] = useState(false);
  const [data, setData] = useState([]);
  const { categories, dataDispatch, URL } = useContext(DataContext);
  const handleClose = () => {
    setIsOpen(false);
  };
  const handleCloseChange = () => {
    setIsOpenChange(false);
  };
  const handleChange = (id, name) => {
    setIsOpenChange(true);
    setData({ id, name });
  };
  const handleDelete = async (id) => {
    let res = await callAPi("post", `${URL}/data/delete-category`, [id]);
    if (!res.status) {
      alert(res.message);
      return;
    }
    if (res.status) {
      let temp = [...categories];
      const i = temp.findIndex((item) => item.id === id);
      temp.splice(i, 1);
      dataDispatch({
        type: "SET_CATEGORIES",
        payload: temp,
      });
      alert(res.message);
    }
  };
  const CustomIconAction = ({ id, name }) => (
    <div style={{ gap: ".75rem", display: "flex" }}>
      <span
        className="IconDataTable Primary"
        style={{ padding: "2px 4px", cursor: "pointer" }}
      >
        <FiEdit size={16} onClick={() => handleChange(id, name)} />
      </span>
      <span
        className="IconDataTable Danger"
        style={{ padding: "2px 4px", cursor: "pointer" }}
        onClick={() => handleDelete(id)}
      >
        <FiTrash2 size={16} />
      </span>
    </div>
  );
  const columnCategories = [
    {
      name: "ID",
      selector: (row) => row.id,
      width: "70px",
      sortable: true,
    },
    {
      name: "Name",
      selector: (row) => (
        <div data-tooltip-id="type" data-tooltip-content={row.type}>
          {row.type}
          <Tooltip
            id="type"
            effect="solid"
            place="top"
            type="dark"
            className="custom-tooltip"
          />
        </div>
      ),
      width: "200px",
    },
    {
      name: "Index",
      selector: (row) => (
        <div style={{ display: "flex", justifyContent: "center" }}>
          <input
            type="number"
            className="InputIndex"
            value={
              row.index !== null && row.index !== undefined ? row.index : 0
            }
            min={0}
          />
        </div>
      ),
      width: "95px",
    },
    {
      name: "Status",
      selector: (row) => (
        <div style={{ display: "flex", justifyContent: "center" }}>
          <Form.Check className="Check" checked={row.status} />
        </div>
      ),
      width: "70px",
    },
    {
      name: "Manage",
      selector: (row) => <CustomIconAction id={row.id} name={row.type} />,
    },
  ];
  return (
    <>
      <div className="ManageCategory">
        <div className="ManageCategory_Header">
          <div className="ManageCategory_Header_Item">
            <div
              className="ManageCategory_Header_Item_Content"
              onClick={() => setIsOpen(true)}
            >
              <div className="ManageCategory_Header_Item_Content_Text">
                Create
              </div>
              <div className="ManageCategory_Header_Item_Content_Icon">
                <FiPlusCircle />
              </div>
            </div>
            {isOpen && <Pop handleClose={handleClose} />}
          </div>
        </div>
        <div className="ManageCategory_Table">
          <DataTable
            title={"Manage Category"}
            columns={columnCategories}
            data={categories}
            fixedHeader={true}
            fixedHeaderScrollHeight="400px"
            pagination
            highlightOnHover={true}
          />
        </div>
        {isOpenChange && (
          <PopChange data={data} handleClose={handleCloseChange} />
        )}
      </div>
    </>
  );
};

const Pop = ({ handleClose }) => {
  const { URL, dataDispatch, categories } = useContext(DataContext);
  const [name, setName] = useState("");

  const handleSubmit = async () => {
    let body = [name];
    if (name === "") {
      alert("Vui lòng nhập đầy đủ thông tin!");
      return;
    }
    let res = await callAPi("post", `${URL}/data/add-category`, body);
    if (!res.status) {
      alert(res.message);
      return;
    }
    if (res.status) {
      console.log(res);
      console.log(categories);
      alert(res.message);
      dataDispatch({
        type: "SET_CATEGORIES",
        payload: [
          ...categories,
          {
            id: res.id,
            type: name,
            index: 0,
            status: false,
          },
        ],
      });
      handleClose();
    }
  };
  return (
    <>
      <div className="Pop">
        <div className="Pop_Container">
          <div className="Pop_Container_Input">
            <input
              type="text"
              value={name}
              placeholder="Tên danh mục..."
              onChange={(e) => setName(e.target.value)}
            />
          </div>
          <div className="Pop_Container_Button">
            <div
              className="Pop_Container_Button_Confirm"
              onClick={handleSubmit}
            >
              Xác nhận
            </div>
            <div className="Pop_Container_Button_Close" onClick={handleClose}>
              Hủy bỏ
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
const PopChange = ({ data, handleClose }) => {
  const { URL, dataDispatch, categories } = useContext(DataContext);
  const [dataCate, setDataCate] = useState(data);

  const handleOnChange = (value, field) => {
    setDataCate((prevData) => ({
      ...prevData,
      [field]: value,
    }));
  };
  const handleSubmit = async () => {
    if (dataCate.name === "") {
      alert("Vui lòng nhập đầy đủ thông tin!");
      return;
    }
    let res = await callAPi("post", `${URL}/data/edit-category-name`, [
      dataCate.id,
      dataCate.name,
    ]);
    if (!res.status) {
      alert(res.message);
      return;
    }
    if (res.status) {
      alert(res.message);
      const updatedCate = categories.map((item) =>
        item.id === dataCate.id ? { ...item, type: dataCate.name } : item
      );
      dataDispatch({
        type: "SET_CATEGORIES",
        payload: updatedCate,
      });
      handleClose();
    }
  };
  return (
    <>
      <div className="Pop">
        <div className="Pop_Container">
          <div className="Pop_Container_Input">
            <input
              type="text"
              value={dataCate.name}
              placeholder="Tên danh mục..."
              onChange={(e) => handleOnChange(e.target.value, "name")}
            />
          </div>
          <div className="Pop_Container_Button">
            <div
              className="Pop_Container_Button_Confirm"
              onClick={handleSubmit}
            >
              Xác nhận
            </div>
            <div className="Pop_Container_Button_Close" onClick={handleClose}>
              Hủy bỏ
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default ManageCategory;
