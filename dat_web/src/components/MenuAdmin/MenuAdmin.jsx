import React, { useState } from "react";
import "./MenuAdmin.scss";
import { RxDashboard } from "react-icons/rx";
import { useNavigate } from "react-router-dom";
import { FiSettings } from "react-icons/fi";
import { PiMessengerLogo } from "react-icons/pi";
import { menu } from "../../App";

const Menu = () => {
  const navigate = useNavigate("");
  const [activeItem, setActiveItem] = useState("news");

  const handleItemClick = (item) => {
    menu.value = item;
    navigate(`/admin/manage`);
    setActiveItem(item);
  };
  const handleChangePage = (item) => {
    setActiveItem(item);
    navigate(`/admin/${item}`);
  };
  return (
    <div className="Dat_Menu">
      <img
        className="Dat_Menu_Logo"
        src="/icons/logo_embody_dark.png"
        alt=""
        onClick={() => {
          navigate("/");
        }}
      />
      <div
        className={`Dat_Menu_Item ${activeItem === "news" ? "Active" : ""}`}
        onClick={(e) => handleItemClick("news")}
      >
        <div className="Dat_Menu_Item_Icon">
          <RxDashboard />
        </div>
        <div className="Dat_Menu_Item_Text">News</div>
      </div>
      <div
        className={`Dat_Menu_Item ${activeItem === "navbar" ? "Active" : ""}`}
        onClick={(e) => handleItemClick("navbar")}
      >
        <div className="Dat_Menu_Item_Icon">
          <RxDashboard />
        </div>
        <div className="Dat_Menu_Item_Text">Navbar</div>
      </div>
      <div
        className={`Dat_Menu_Item ${activeItem === "category" ? "Active" : ""}`}
        onClick={(e) => handleItemClick("category")}
      >
        <div className="Dat_Menu_Item_Icon">
          <RxDashboard />
        </div>
        <div className="Dat_Menu_Item_Text">Category</div>
      </div>
      <div
        className={`Dat_Menu_Item ${activeItem === "chat" ? "Active" : ""}`}
        id="chat"
        onClick={(e) => handleChangePage("chat")}
      >
        <div className="Dat_Menu_Item_Icon">
          <PiMessengerLogo />
        </div>
        <div className="Dat_Menu_Item_Text">Message</div>
      </div>
      <div
        className={`Dat_Menu_Item ${activeItem === "setting" ? "Active" : ""}`}
        id="Setting"
        onClick={() => handleChangePage("setting")}
      >
        <div className="Dat_Menu_Item_Icon">
          <FiSettings />
        </div>
        <div className="Dat_Menu_Item_Text">Setting</div>
      </div>
    </div>
  );
};

export default Menu;
