import { useState, useEffect, useContext } from "react";
import "./ManagePost.scss";
import { Tooltip } from "react-tooltip";
import "react-tooltip/dist/react-tooltip.css";
import DataTable from "react-data-table-component";
import Form from "react-bootstrap/Form";
import { callAPi } from "../../services/UserService";
import dayjs from "dayjs";
import Toast from "../Toast/Toast";
import {
  FiChevronRight,
  FiEdit,
  FiPlusCircle,
  FiSearch,
  FiTrash2,
  FiX,
} from "react-icons/fi";
import { useNavigate } from "react-router-dom";
import DatePicker from "../Litepicker/Litepicker";
import { DataContext } from "../Context/DataContext";
import { menu } from "../../App";

const ManagePost = () => {
  const navigate = useNavigate("");
  const { URL, dataDispatch, categories } = useContext(DataContext);
  const [isOpenSortTable, setIsOpenSortTable] = useState(false);
  const [isOpenSortCategory, setIsOpenSortCategory] = useState(false);
  const [isOpenPopup, setIsOpenPopup] = useState("");
  const menuItems = ["News", "Navbar", "Category"];
  const [filterTableValue, setFilterTableValue] = useState(menuItems[0]);
  const [filterValue, setFilterValue] = useState("");
  const [idCategory, setIdCategory] = useState("");
  const [tempUpdate, setTempUpdate] = useState({});

  const [news, setNews] = useState([]);
  const [totalNews, setTotalNews] = useState([]);
  const [preCategories, setPreCategories] = useState([]);
  const [selectedDates, setSelectedDates] = useState({
    start: null,
    end: null,
  });

  const [toastMessage, setToastMessage] = useState("");
  const [showToast, setShowToast] = useState(false);

  // news,totalnews
  const getNews = async (id) => {
    let res;
    if (id === 0) {
      res = await callAPi("get", `${URL}/data/get-highlight-news`);
    } else if (id === -1) {
      res = await callAPi("get", `${URL}/data/get-news-by-null-category`);
    } else {
      res = await callAPi("get", `${URL}/data/get-news-by-category/${id}`);
    }

    if (!res.status) {
      showToastMessage("Lấy bài báo thất bại");
      return;
    }
    console.log(res.data);
    setIdCategory(id);
    let newData = res.data.map((item, index) => {
      return {
        ...item,
        i: index,
      };
    });
    setTotalNews(newData);
    setNews(newData);
  };

  // news,totalnews
  const getNavbar = async () => {
    let res = await callAPi("get", `${URL}/data/get-admin-menu`);

    if (!res.status) {
      showToastMessage("Lấy dữ liệu thất bại");
      return;
    }
    let newData = res.data.map((item, index) => {
      return {
        ...item,
        i: index,
      };
    });
    setTotalNews(newData);
    setNews(newData);
  };

  const getCategories = async () => {
    let res = await callAPi("get", `${URL}/data/get-all-categories`);
    if (!res.status) {
      showToastMessage("get categories fail");
      return;
    }
    let newData = res.data.map((item, index) => {
      return {
        ...item,
        i: index + 1,
      };
    });
    dataDispatch({
      type: "SET_CATEGORIES",
      payload: newData,
    });
    setPreCategories(newData);
    getNews(newData[0].id);
    setFilterValue(newData[0].type);
  };

  useEffect(() => {
    console.log(menu.value);
    setFilterTableValue(menu.value);
    if (menu.value === "Navbar") {
      getNavbar();
    }

    if (menu.value === "News") {
      getNews(0);
    }
  }, [menu.value]);

  useEffect(() => {
    getCategories();
  }, []);

  useEffect(() => {
    if (selectedDates.start !== null && selectedDates.end !== null) {
      handleSearchByDate();
    }
  }, [selectedDates]);

  const handleOpenPopup = (type, value) => {
    if (type === "table") {
      setIsOpenSortTable(value);
    } else if (type === "category") {
      setIsOpenSortCategory(value);
    }
  };

  const showToastMessage = (message) => {
    setToastMessage(message);
    setShowToast(true);
    setTimeout(() => {
      setShowToast(false);
    }, 1500);
  };

  const handleDateSelect = (startDate, endDate) => {
    setSelectedDates({ start: startDate, end: endDate });
  };


  const handleFilter = (id, type) => {
    setFilterValue(type);
    getNews(id);
  };

  const handleEditPublish = async (e, id) => {
    e.preventDefault();
    const newValue = e.target.checked;
    let temp;
    let i;
    let res;
    if (filterTableValue === "Navbar") {
      temp = [...news];
      i = temp.findIndex((item) => item.id === id);
      res = await callAPi("post", `${URL}/data/edit-publish-menu`, [
        temp[i].id,
        newValue,
      ]);
    } else if (filterTableValue === "Category") {
      temp = [...categories];

      i = temp.findIndex((item) => item.id === id);
      if (temp[i].id === 0) {
        return;
      }
      res = await callAPi("post", `${URL}/data/edit-category-status`, [
        temp[i].id,
        newValue,
      ]);
    } else {
      temp = [...news];
      i = temp.findIndex((item) => item.id === id);
      if (filterValue === "Highlight news") {
        let body = [temp[i].id, newValue];
        res = await callAPi(
          "post",
          `${URL}/data/eidt-news-highlight-status`,
          body
        );
      } else {
        let body = [temp[i].id, idCategory, newValue];
        res = await callAPi("post", `${URL}/data/edit-publish-news`, body);
      }
    }

    if (!res.status) {
      showToastMessage(res.message);
      setNews(totalNews);
      return;
    }
    showToastMessage(res.message);
    temp[i] = {
      ...temp[i],
      status: newValue,
    };
    if (filterTableValue === "Category") {
      dataDispatch({
        type: "SET_CATEGORIES",
        payload: temp,
      });
      setPreCategories(temp);
    } else {
      setNews([...temp]);
    }
  };

  const handleEditIndex = async (i) => {
    if (news[i].index === totalNews[i].index) return;
    const result = totalNews.filter((item) => item.index === news[i].index);
    if (result.length > 0 && news[i].index !== 0) {
      showToastMessage("Duplicate index");
      setNews(totalNews);
    } else {
      let res;
      if (filterTableValue === "Navbar") {
        res = await callAPi("post", `${URL}/data/edit-index-menu`, [
          news[i].id,
          news[i].index,
        ]);
      } else if (filterTableValue === "Category") {
        res = await callAPi("post", `${URL}/data/edit-category-index`, [
          news[i].id,
          news[i].index,
        ]);
      } else if (filterTableValue === "News") {
        if (filterValue === "Highlight news") {
          let body = [news[i].id, news[i].index];
          res = await callAPi(
            "post",
            `${URL}/data/eidt-news-highlight-index`,
            body
          );
        } else {
          let body = [news[i].id, idCategory, news[i].index];
          res = await callAPi("post", `${URL}/data/edit-index-news`, body);
        }
      }

      if (!res.status) {
        showToastMessage(res.message);
        setNews(totalNews);
        return;
      }
      showToastMessage(res.message);
      let temp = [...totalNews];
      const i_news = temp.findIndex((item) => item.id === news[i].id);
      temp[i_news] = {
        ...temp[i_news],
        index: news[i].index,
      };
      setTotalNews([...temp]);
    }
  };

  const handleEditIndexCategory = async (i) => {
    if (categories[i].index === preCategories[i].index) return;
    const result = preCategories.filter(
      (item) => item.index === categories[i].index
    );
    if (result.length > 0 && categories[i].index !== 0) {
      showToastMessage("Duplicate index");
      dataDispatch({ type: "SET_CATEGORIES", payload: preCategories });
      return;
    }
    let res = await callAPi("post", `${URL}/data/edit-category-index`, [
      categories[i].id,
      categories[i].index,
    ]);

    if (!res.status) {
      showToastMessage(res.message);
      dataDispatch({ type: "SET_CATEGORIES", payload: preCategories });
      return;
    }
    showToastMessage(res.message);
    let temp = [...categories];
    const i_category = temp.findIndex((item) => item.id === categories[i].id);
    temp[i_category] = {
      ...temp[i_category],
      index: categories[i].index,
    };
    setPreCategories([...temp]);
  };
  const handleEditHighlight = async (e, id) => {
    e.preventDefault();
    const newValue = e.target.checked;
    let res;
    if (newValue) {
      res = await callAPi("post", `${URL}/data/add-news-highlight`, [id]);
    } else {
      res = await callAPi("post", `${URL}/data/delete-news-highlight`, [id]);
    }

    if (!res.status) {
      showToastMessage(res.message);
      setNews(totalNews);
      return;
    }
    showToastMessage(res.message);

    let temp = [...news];

    const i = temp.findIndex((item) => item.id === id);
    temp[i] = {
      ...temp[i],
      is_highlighted: newValue,
    };
    setNews([...temp]);
  };

  const handleAdd = async () => {
    if (filterTableValue === "News") {
      navigate("/admin/createarticle");
    } else {
      setIsOpenPopup("add");
    }
  };

  const handleSearch = (e) => {
    if (news === undefined) return;
    const newValue = e.target.value;
    let news_search = [];
    let count = 0;
    totalNews.map((item) => {
      if (item.title.toLowerCase().includes(newValue.toLowerCase())) {
        news_search = [...news_search, { ...item, i: count }];
        count += 1;
      }
    });
    setNews(news_search);
  };

  const handleSearchByDate = () => {
    const start = new Date(selectedDates.start);
    const end = new Date(selectedDates.end);
    let count = 0;
    let temp = [];
    totalNews.map((item) => {
      const date = item.updated_date.split("T")[0];
      const formatdate = new Date(date);
      if (formatdate >= start && formatdate <= end) {
        temp = [...temp, { ...item, i: count }];
        count += 1;
      }
    });
    setNews([...temp]);
  };

  const handleOnchangeIndex = (e, id) => {
    e.preventDefault();
    const newValue = e.target.value;
    let temp;
    temp = [...news];

    const i = temp.findIndex((item) => item.id === id);
    temp[i] = {
      ...temp[i],
      index: parseInt(newValue, 10) || 0,
    };
    setNews([...temp]);
  };

  const handleOnchangeIndexCategory = (e, id) => {
    e.preventDefault();
    const newValue = e.target.value;
    let temp;
    temp = [...categories];
    const i = temp.findIndex((item) => item.id === id);
    temp[i] = {
      ...temp[i],
      index: parseInt(newValue, 10) || 0,
    };
    dataDispatch({
      type: "SET_CATEGORIES",
      payload: [...temp],
    });
  };

  const handleEdit = async (id) => {
    if (filterTableValue === "Navbar") {
      const menuItem = news.find((item) => item.id === id);
      setTempUpdate({
        id: id,
        name: menuItem.name,
        link: menuItem.link,
      });
      setIsOpenPopup("update");
    } else if (filterTableValue === "Category") {
      const categoryItem = categories.find((item) => item.id === id);
      setTempUpdate({
        id: id,
        name: categoryItem.type,
      });
      setIsOpenPopup("update");
    } else {
      navigate(`/admin/editarticle/${id}`);
    }
  };

  const handleDelete = async (id) => {
    let res;
    if (filterTableValue === "Navbar") {
      res = await callAPi("post", `${URL}/data/delete-menu`, [id]);
    } else if (filterTableValue === "Category") {
      res = await callAPi("post", `${URL}/data/delete-category`, [id]);
    } else {
      if (filterValue === "Highlight news") {
        res = await callAPi("post", `${URL}/data/delete-news-highlight`, [id]);
      } else {
        let body = [id, idCategory];
        res = await callAPi("post", `${URL}/data/delete-news`, body);
      }
    }

    if (!res.status) {
      showToastMessage(res.message);
      return;
    }
    showToastMessage(res.message);

    if (filterTableValue === "Category") {
      let temp = [...categories];
      const i = temp.findIndex((item) => item.id === id);
      temp.splice(i, 1);
      dataDispatch({
        type: "SET_CATEGORIES",
        payload: [...temp],
      });
      setPreCategories([...temp]);
    } else {
      let temp = [...news];
      const i = temp.findIndex((item) => item.id === id);
      temp.splice(i, 1);
      setNews([...temp]);
      setTotalNews([...temp]);
    }
  };
  const CustomIconAction = ({ id }) => (
    <div style={{ gap: ".75rem", display: "flex" }}>
      {filterTableValue === "News" && filterValue === "Highlight news" ? (
        <></>
      ) : (
        <span
          className="IconDataTable Primary"
          style={{ padding: "2px 4px", cursor: "pointer" }}
          onClick={() => handleEdit(id)}
        >
          <FiEdit size={16} />
        </span>
      )}

      <span
        className="IconDataTable Danger"
        style={{ padding: "2px 4px", cursor: "pointer" }}
        onClick={() => handleDelete(id)}
      >
        <FiTrash2 size={16} />
      </span>
    </div>
  );

  const columnNews = [
    {
      name: "ID",
      selector: (row) => row.i + 1,
      width: "70px",
      sortable: true,
    },
    {
      name: "Title",
      selector: (row) => (
        <div data-tooltip-id="title" data-tooltip-content={row.title}>
          {row.title}
          <Tooltip
            id="title"
            effect="solid"
            place="top"
            type="dark"
            className="custom-tooltip"
          />
        </div>
      ),
      width: "200px",
    },
    {
      name: "Content",
      selector: (row) => (
        <div data-tooltip-id="content" data-tooltip-content={row.content}>
          {row.content}
          <Tooltip
            id="content"
            effect="solid"
            place="top"
            type="dark"
            className="custom-tooltip"
          />
        </div>
      ),
      width: "200px",
    },
    {
      name: "Image",
      selector: (row) => (
        <div data-tooltip-id="image_url" data-tooltip-content={row.image_url}>
          {row.image_url}
          <Tooltip
            id="image_url"
            effect="solid"
            place="top"
            type="dark"
            className="custom-tooltip"
          />
        </div>
      ),
      width: "150px",
    },
    {
      name: "Index",
      selector: (row) => (
        <div style={{ display: "flex", justifyContent: "center" }}>
          <input
            type="number"
            className="InputIndex"
            value={
              row.index !== null && row.index !== undefined ? row.index : ""
            }
            onChange={(e) => {
              handleOnchangeIndex(e, row.id);
            }}
            onBlur={() => {
              handleEditIndex(row.i);
            }}
            min={0}
          />
        </div>
      ),
      width: "95px",
    },
    {
      name: "Status",
      selector: (row) => (
        <div style={{ display: "flex", justifyContent: "center" }}>
          <Form.Check
            className="Check"
            checked={row.status}
            onChange={(e) => {
              handleEditPublish(e, row.id);
            }}
          />
        </div>
      ),
      width: "70px",
    },
    {
      name: "HighLight",
      selector: (row) => (
        <div style={{ display: "flex", justifyContent: "center" }}>
          <Form.Check
            className="Check"
            checked={row.is_highlighted}
            onChange={(e) => {
              handleEditHighlight(e, row.id);
            }}
          />
        </div>
      ),
      width: "90px",
    },
    {
      name: "Date Publish",
      selector: (row) => {
        if (row.published_date === undefined || row.published_date === null)
          return null;
        return dayjs(row.published_date.split("T")[0]).format("DD-MM-YYYY");
      },
      width: "150px",
    },
    {
      name: "Date Update",
      selector: (row) => {
        if (row.updated_date === undefined || row.updated_date === null)
          return null;

        return dayjs(row.updated_date.split("T")[0]).format("DD-MM-YYYY");
      },
      width: "150px",
    },
    {
      name: "Manage",
      selector: (row) => <CustomIconAction id={row.id} />,
    },
  ];
  const columnNewsHighlight = [
    {
      name: "ID",
      selector: (row) => row.i + 1,
      width: "70px",
      sortable: true,
    },
    {
      name: "Categories",
      selector: (row) => {
        if (row.categories === undefined) return null;
        let categoriesString;
        if (row.categories === null) {
          categoriesString = "----";
        } else {
          categoriesString = row.categories
            .map((category) => category.category_name)
            .join(", ");
        }

        return (
          <div
            data-tooltip-id="categories"
            data-tooltip-content={categoriesString}
          >
            {categoriesString}
            <Tooltip
              id="categories"
              effect="solid"
              place="top"
              type="dark"
              className="custom-tooltip"
            />
          </div>
        );
      },
      width: "170px",
      sortable: true,
    },
    {
      name: "Title",
      selector: (row) => (
        <div data-tooltip-id="title" data-tooltip-content={row.title}>
          {row.title}
          <Tooltip
            id="title"
            effect="solid"
            place="top"
            type="dark"
            className="custom-tooltip"
          />
        </div>
      ),
      width: "200px",
    },
    {
      name: "Content",
      selector: (row) => (
        <div data-tooltip-id="content" data-tooltip-content={row.content}>
          {row.content}
          <Tooltip
            id="content"
            effect="solid"
            place="top"
            type="dark"
            className="custom-tooltip"
          />
        </div>
      ),
      width: "200px",
    },
    {
      name: "Image",
      selector: (row) => (
        <div data-tooltip-id="image_url" data-tooltip-content={row.image_url}>
          {row.image_url}
          <Tooltip
            id="image_url"
            effect="solid"
            place="top"
            type="dark"
            className="custom-tooltip"
          />
        </div>
      ),
      width: "150px",
    },
    {
      name: "Index",
      selector: (row) => (
        <div style={{ display: "flex", justifyContent: "center" }}>
          <input
            type="number"
            className="InputIndex"
            value={
              row.index !== null && row.index !== undefined ? row.index : ""
            }
            onChange={(e) => {
              handleOnchangeIndex(e, row.id);
            }}
            onBlur={() => {
              handleEditIndex(row.i);
            }}
            min={0}
          />
        </div>
      ),
      width: "95px",
    },
    {
      name: "Status",
      selector: (row) => (
        <div style={{ display: "flex", justifyContent: "center" }}>
          <Form.Check
            className="Check"
            checked={row.status}
            onChange={(e) => {
              handleEditPublish(e, row.id);
            }}
          />
        </div>
      ),
      width: "70px",
    },
    {
      name: "Date Publish",
      selector: (row) => {
        if (row.published_date === undefined || row.published_date === null)
          return null;
        return dayjs(row.published_date.split("T")[0]).format("DD-MM-YYYY");
      },
      width: "150px",
    },
    {
      name: "Date Update",
      selector: (row) => {
        if (row.updated_date === undefined || row.updated_date === null)
          return null;

        return dayjs(row.updated_date.split("T")[0]).format("DD-MM-YYYY");
      },
      width: "150px",
    },
    {
      name: "Manage",
      selector: (row) => <CustomIconAction id={row.id_news} />,
    },
  ];
  const columnNavbar = [
    {
      name: "ID",
      selector: (row) => row.i + 1,
      width: "70px",
      sortable: true,
    },
    {
      name: "Name",
      selector: (row) => (
        <div data-tooltip-id="name" data-tooltip-content={row.name}>
          {row.name}
          <Tooltip
            id="name"
            effect="solid"
            place="top"
            type="dark"
            className="custom-tooltip"
          />
        </div>
      ),
      width: "200px",
    },
    {
      name: "link",
      selector: (row) => (
        <div data-tooltip-id="link" data-tooltip-content={row.link}>
          {row.link}
          <Tooltip
            id="link"
            effect="solid"
            place="top"
            type="dark"
            className="custom-tooltip"
          />
        </div>
      ),
      width: "200px",
    },
    {
      name: "Index",
      selector: (row) => (
        <div style={{ display: "flex", justifyContent: "center" }}>
          <input
            type="number"
            className="InputIndex"
            value={
              row.index !== null && row.index !== undefined ? row.index : 0
            }
            onChange={(e) => {
              handleOnchangeIndex(e, row.id);
            }}
            onBlur={() => {
              handleEditIndex(row.i);
            }}
            min={0}
          />
        </div>
      ),
      width: "95px",
    },
    {
      name: "Status",
      selector: (row) => (
        <div style={{ display: "flex", justifyContent: "center" }}>
          <Form.Check
            className="Check"
            checked={row.status}
            onChange={(e) => {
              handleEditPublish(e, row.id);
            }}
          />
        </div>
      ),
      width: "70px",
    },
    {
      name: "Manage",
      selector: (row) => <CustomIconAction id={row.id} />,
    },
  ];
  const columnCategories = [
    {
      name: "ID",
      selector: (row) => row.i,
      width: "70px",
      sortable: true,
    },
    {
      name: "Name",
      selector: (row) => (
        <div data-tooltip-id="type" data-tooltip-content={row.type}>
          {row.type}
          <Tooltip
            id="type"
            effect="solid"
            place="top"
            type="dark"
            className="custom-tooltip"
          />
        </div>
      ),
      width: "200px",
    },
    {
      name: "Index",
      selector: (row) => (
        <div style={{ display: "flex", justifyContent: "center" }}>
          <input
            type="number"
            className="InputIndex"
            value={
              row.index !== null && row.index !== undefined ? row.index : 0
            }
            onChange={(e) => {
              handleOnchangeIndexCategory(e, row.id);
            }}
            onBlur={() => {
              handleEditIndexCategory(row.i);
            }}
            min={0}
          />
        </div>
      ),
      width: "95px",
    },
    {
      name: "Status",
      selector: (row) => (
        <div style={{ display: "flex", justifyContent: "center" }}>
          <Form.Check
            className="Check"
            checked={row.status}
            onChange={(e) => {
              handleEditPublish(e, row.id);
            }}
          />
        </div>
      ),
      width: "70px",
    },
    {
      name: "Manage",
      selector: (row) => <CustomIconAction id={row.id} />,
    },
  ];
  const handleAfterChange = (type, action, id, name, link) => {
    if (type === "Navbar") {
      let temp;
      if (action === "add") {
        temp = [
          ...news,
          {
            id: id,
            i: news.length,
            name: name,
            link: link,
            index: 0,
            status: false,
          },
        ];
      } else if (action === "edit") {
        temp = [...news];
        let index = news.findIndex((item) => item.id === id);
        temp[index] = {
          ...temp[index],
          name: name,
          link: link,
        };
      }
      setNews(temp);
      setTotalNews(temp);
    } else if (type === "Category") {
      let temp;
      if (action === "add") {
        temp = [...categories, { id: id, i: categories.length, type: name }];
      } else if (action === "edit") {
        temp = [...categories];
        let index = temp.findIndex((item) => item.id === id);
        temp[index] = {
          ...temp[index],
          type: name,
        };
      }
      dataDispatch({
        type: "SET_CATEGORIES",
        payload: temp,
      });
      setPreCategories(temp);
    }
  };

  return (
    <div className="DAT_ManagePost">
      {showToast && (
        <Toast message={toastMessage} onClose={() => setShowToast(false)} />
      )}

      <div className="DAT_ManagePost_Container">
        <div className="DAT_ManagePost_Container_Header">
          <div className="DAT_ManagePost_Container_Header_Item">
            <div
              onClick={() => {
                setIsOpenSortCategory(false);
                setIsOpenSortTable(!isOpenSortTable);
              }}
              className="DAT_ManagePost_Container_Header_Item_Content"
            >
              <div className="DAT_ManagePost_Container_Header_Item_Content_Text">
                {filterTableValue}
              </div>
              <div
                className={`DAT_ManagePost_Container_Header_Item_Content_Icon ${
                  isOpenSortTable ? "Show" : ""
                }`}
              >
                <FiChevronRight />
              </div>
            </div>

            {isOpenPopup === "add" && (
              <PopupAdd
                type={filterTableValue}
                setOpen={setIsOpenPopup}
                showToastMessage={showToastMessage}
                handleAfterChange={handleAfterChange}
              />
            )}
            {isOpenPopup === "update" && (
              <PopupChange
                type={filterTableValue}
                setOpen={setIsOpenPopup}
                body={tempUpdate}
                showToastMessage={showToastMessage}
                handleAfterChange={handleAfterChange}
              />
            )}
            <DropDownMenu
              isOpen={isOpenSortTable}
              data={menuItems}
              handleOpenPopup={handleOpenPopup}
            />
          </div>
          <div
            className="DAT_ManagePost_Container_Header_Item"
            onClick={() => handleAdd()}
          >
            <div className="DAT_ManagePost_Container_Header_Item_Content">
              <div className="DAT_ManagePost_Container_Header_Item_Content_Text">
                Create
              </div>
              <div className="DAT_ManagePost_Container_Header_Item_Content_Icon">
                <FiPlusCircle />
              </div>
            </div>
          </div>
        </div>
        {filterTableValue === "News" && (
          <div className="DAT_ManagePost_Container_Sort">
            <div className="DAT_ManagePost_Container_Sort_Item">
              <div
                className="DAT_ManagePost_Container_Sort_Item_Category"
                onClick={() => setIsOpenSortCategory(!isOpenSortCategory)}
              >
                <div className="DAT_ManagePost_Container_Sort_Item_Category_Content">
                  Category
                </div>
                <div
                  className={`DAT_ManagePost_Container_Sort_Item_Category_Icon ${
                    isOpenSortCategory ? "Show" : ""
                  }`}
                >
                  <FiChevronRight />
                </div>
              </div>
              <DropDownMenu
                statusChange={true}
                isOpen={isOpenSortCategory}
                data={categories}
                setItem={setFilterValue}
                handleFilter={handleFilter}
                handleOpenPopup={handleOpenPopup}
              />
            </div>
            <div className="DAT_ManagePost_Container_Sort_Item">
              <div className="DAT_ManagePost_Container_Sort_Item_Search">
                <div className="DAT_ManagePost_Container_Sort_Item_Search_Icon">
                  <FiSearch />
                </div>
                <input
                  type="text"
                  placeholder="Search..."
                  onChange={handleSearch}
                />
              </div>
            </div>
            <div className="DAT_ManagePost_Container_Sort_Item">
              <div className="DAT_ManagePost_Container_Sort_Item_Date">
                <DatePicker onDateSelect={handleDateSelect} />
              </div>
            </div>
          </div>
        )}

        <div className="DAT_ManagePost_Container_Table">
          <DataTable
            title={
              filterTableValue === "News"
                ? filterValue
                : `${filterTableValue} table`
            }
            columns={
              filterTableValue === "News"
                ? filterValue !== "Highlight news" &&
                  news.length > 0 &&
                  news[0]["categories"] === undefined
                  ? columnNews
                  : columnNewsHighlight
                : filterTableValue === "Navbar"
                ? columnNavbar
                : columnCategories
            }
            data={
              filterTableValue === "Category"
                ? categories.filter((row) => row.i !== 0)
                : news
            }
            fixedHeader={true}
            fixedHeaderScrollHeight="400px"
            pagination
            highlightOnHover={true}
          />
        </div>
      </div>
    </div>
  );
};

const DropDownMenu = ({
  isOpen,
  data,
  handleFilter,
  handleFilterTable,
  statusChange,
  handleOpenPopup,
}) => {
  const [isOpenPopDelete, setIsOpenPopDelete] = useState(false);
  const [isOpenPopChange, setIsOpenPopChange] = useState(false);

  return (
    <>
      <div className={`DropDownMenu ${isOpen ? "Show" : ""}`}>
        {data && statusChange ? (
          <>
            <div
              className="DropDownMenu_Null"
              onClick={() => {
                handleFilter(0, "Highlight news");
                handleOpenPopup("category", false);
              }}
            >
              Highlight news
            </div>
            {data.map((item, index) => (
              <div className="DropDownMenu_Item" key={index}>
                <div
                  className="DropDownMenu_Item_Text"
                  onClick={() => {
                    handleFilter(item.id, item.type);
                    handleOpenPopup("category", false);
                  }}
                >
                  {item.type}
                </div>
              </div>
            ))}
            <div
              className="DropDownMenu_Null"
              onClick={() => {
                handleFilter(-1, "không có danh mục");
                handleOpenPopup("category", false);
              }}
            >
              Không danh mục
            </div>
          </>
        ) : (
          data.map((item, index) => (
            <div className="DropDownMenu_Item" key={index}>
              <div
                className="DropDownMenu_Item_Text"
                onClick={() => {
                  handleFilterTable(item);
                  handleOpenPopup("table", false);
                }}
              >
                {item}
              </div>
            </div>
          ))
        )}
        {isOpenPopChange ? <PopupChange setOpen={setIsOpenPopChange} /> : <></>}
        {isOpenPopDelete ? <PopupDelete setOpen={setIsOpenPopDelete} /> : <></>}
      </div>
    </>
  );
};

const PopupDelete = ({ category, setOpen, id }) => {
  const { URL, categories, dataDispatch } = useContext(DataContext);
  const handleDelete = async () => {
    let res = await callAPi("post", `${URL}/data/delete-category`, [id]);
    if (res.status) {
      const filteredData = categories.filter((obj) => obj.id !== id);
      dataDispatch({
        type: "SET_CATEGORIES",
        payload: filteredData,
      });
      alert(res.message);
      setOpen(false);
    } else {
      alert(res.message);
    }
  };
  return (
    <>
      <div className="Popup">
        <div className="Popup_Ask">
          <div className="Popup_Ask_Text">
            Bạn có chắc chắn xóa danh mục <span>{category}</span> này không?
          </div>
          <div className="Popup_Ask_Btn">
            <button onClick={() => handleDelete()}>Xác nhận</button>
            <button onClick={() => setOpen(false)}>Hủy</button>
          </div>
        </div>
      </div>
    </>
  );
};

const PopupAdd = ({ setOpen, type, showToastMessage, handleAfterChange }) => {
  const [name, setName] = useState("");
  const [link, setLink] = useState("");
  const { URL } = useContext(DataContext);

  const handleAdd = async () => {
    if (type === "Navbar") {
      if (name === "" || link === "") {
        showToastMessage("Vui lòng nhập đầy đủ thông tin");
        return;
      }
      let res = await callAPi("post", `${URL}/data/add-menu`, [name, link]);
      if (!res.status) {
        showToastMessage(res.message);
        return;
      }
      handleAfterChange(type, "add", res.id, name, link);
      showToastMessage(res.message);
      setOpen("");
    } else if (type === "Category") {
      if (name === "") {
        showToastMessage("Vui lòng nhập đầy đủ thông tin");
        return;
      }
      let res = await callAPi("post", `${URL}/data/add-category`, [name]);
      if (!res.status) {
        showToastMessage(res.message);
        return;
      }
      handleAfterChange(type, "add", res.id, name);
      showToastMessage(res.message);
      setOpen("");
    }
  };

  return (
    <div className="Popup">
      <div className="Popup_Add">
        <div className="Popup_Add_Close">
          <FiX onClick={() => setOpen("")} />
        </div>
        {type === "Navbar" ? (
          <div className="Popup_Add_Input">
            <input
              value={name}
              onChange={(e) => setName(e.target.value)}
              type="text"
              placeholder="Tên danh mục..."
            />
            <input
              value={link}
              onChange={(e) => setLink(e.target.value)}
              type="text"
              placeholder="Đường dẫn..."
            />
          </div>
        ) : (
          <div className="Popup_Add_Input">
            <input
              value={name}
              onChange={(e) => setName(e.target.value)}
              type="text"
              placeholder="Tên danh mục..."
            />
          </div>
        )}

        <div className="Popup_Add_Btn">
          <button onClick={handleAdd}>Lưu</button>
        </div>
      </div>
    </div>
  );
};

const PopupChange = ({
  setOpen,
  type,
  body,
  showToastMessage,
  handleAfterChange,
}) => {
  const [data, setData] = useState(body);

  const handleOnChange = (value, field) => {
    setData((prevData) => ({
      ...prevData,
      [field]: value,
    }));
  };

  const { URL } = useContext(DataContext);

  const handleChange = async () => {
    let res;
    if (type === "Navbar" && data.name !== undefined) {
      if (data.name === "" && data.link === "") {
        showToastMessage("Vui lòng nhập đủ thông tin!");
        return;
      }
      res = await callAPi("post", `${URL}/data/edit-name-menu`, [
        data.id,
        data.name,
        data.link,
      ]);
      if (!res.status) {
        showToastMessage(res.message);
        return;
      }

      showToastMessage(res.message);

      handleAfterChange(type, "edit", data.id, data.name, data.link);

      setOpen("");
    } else if (type === "Category") {
      if (data.name === "") {
        showToastMessage("Vui lòng nhập tên danh mục!");
        return;
      }

      res = await callAPi("post", `${URL}/data/edit-category-name`, [
        data.id,
        data.name,
      ]);
      if (!res.status) {
        showToastMessage(res.message);
        return;
      }
      showToastMessage(res.message);

      handleAfterChange(type, "edit", data.id, data.name);
      setOpen("");
    }
  };

  return (
    <div className="Popup">
      <div className="Popup_Add">
        <div className="Popup_Add_Close">
          <FiX onClick={() => setOpen(false)} />
        </div>
        {type === "Navbar" &&
        data.name !== undefined &&
        data.link !== undefined ? (
          <div className="Popup_Add_Input">
            <input
              value={data.name}
              onChange={(e) => handleOnChange(e.target.value, "name")}
              type="text"
              placeholder="Tên danh mục..."
            />
            <input
              value={data.link}
              onChange={(e) => handleOnChange(e.target.value, "link")}
              type="text"
              placeholder="Đường dẫn..."
            />
          </div>
        ) : (
          <div className="Popup_Add_Input">
            <input
              value={data.name}
              onChange={(e) => handleOnChange(e.target.value, "name")}
              type="text"
              placeholder="Tên danh mục..."
            />
          </div>
        )}

        <div className="Popup_Add_Btn">
          <button onClick={handleChange}>Lưu</button>
        </div>
      </div>
    </div>
  );
};
export default ManagePost;
