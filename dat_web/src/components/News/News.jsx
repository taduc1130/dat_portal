import React, { useContext } from "react";
import "./News.scss";
import { FiArrowRight } from "react-icons/fi";
import { AuthContext } from "../Context/AuthContext";
import {
  extractAfterDateTimeSuffix,
  extractBeforeDateTimeSuffix,
  formatDateToDDMMYYYY,
} from "../Format/Format";
import { useNavigate } from "react-router-dom";
const News = ({ id, image, title, desc, date }) => {
  const { URL } = useContext(AuthContext);
  const navigate = useNavigate("");
  const currentDate = new Date();
  const day = currentDate.getDate();
  const month = currentDate.getMonth() + 1;
  const year = currentDate.getFullYear();
  const formattedDate = `${day}/${month}/${year}`;

  return (
    <>
      <div className="News">
        <div className="News_Container">
          {id ? (
            <div
              className="News_Container_Left"
              style={{
                backgroundImage: `url("${URL}/images/${extractAfterDateTimeSuffix(
                  title
                )}/${image}")`,
              }}
            >
              <div
                style={{
                  backgroundImage: `url("${URL}/images/${extractAfterDateTimeSuffix(
                    title
                  )}/${image}")`,
                }}
                className="News_Container_Left_Image"
              ></div>
            </div>
          ) : (
            <div
              className="News_Container_Left"
              style={{
                backgroundImage: `url("${URL}/images/${extractAfterDateTimeSuffix(
                  title
                )}/${image}")`,
              }}
            >
              <div
                style={{
                  backgroundImage: `url("${image}")`,
                }}
                className="News_Container_Left_Image"
              ></div>
            </div>
          )}
          <div className="News_Container_Right">
            <div className="News_Container_Right_Content">
              {id ? (
                <div
                  className="News_Container_Right_Content_Title"
                  onClick={() => navigate(`/article-detail/${id}`)}
                >
                  {extractBeforeDateTimeSuffix(title)}
                </div>
              ) : (
                <div className="News_Container_Right_Content_Title">
                  {title}
                </div>
              )}

              <div className="News_Container_Right_Content_Desc">{desc}</div>
            </div>
            <div className="News_Container_Right_Footer">
              <div className="News_Container_Right_Footer_DateTime">
                {date ? formatDateToDDMMYYYY(date) : formattedDate}
              </div>
              <div className="News_Container_Right_Footer_Arrow">
                <FiArrowRight />
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default News;
