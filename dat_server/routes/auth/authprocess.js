require("dotenv").config();
const jwt = require("jsonwebtoken");
const db = require("../../models/postgresql");
const bcrypt = require('bcrypt');
const saltRounds = 10; // Number of salt rounds for hashing

const EMAIL_ACCOUNT = process.env.EMAIL_ACCOUNT;
const EMAIL_PASSWORD = process.env.EMAIL_PASSWORD;
const SECRET_KEY = process.env.SECRET_KEY;
// const hashPassword = async (password) => {
//   try {
//       const hashedPassword = await bcrypt.hash(password, saltRounds);
//       return hashedPassword;
//   } catch (error) {
//       console.error('Error hashing password:', error);
//   }
// };

const checkValidAdmin = async (name, pass) => {
  return new Promise(async (resolve, reject) => {
    try {
      let res = await db.SELECT(
        "*",
        "func_adminlogin('" + name + "')"
      );  
      if (res.rows.length == 0) resolve({ status: false, message: "không tìm thấy tên tài khoản" });
      bcrypt.compare(pass, res.rows[0].password, (err, result) => {
        if (result) {
          content = {
            name: name
          };
          let asscessToken = jwt.sign(content, SECRET_KEY);
          resolve({ status: true,username : res.rows[0].username, asscessToken });
        } else resolve({ status: false, message: "sai mật khẩu" });
    }); 
      
    } catch (error) {
      resolve({ status: false, code: 255, message: "Error System" });
    }
  });
};
const LoginWhenIsToken = async (name) => {
  return new Promise(async (resolve, reject) => {
    try {
      let res = await db.SELECT(
        "*",
        "func_adminloginwhentoken('" + name +  "')"
      );
      resolve({ status: true, username: res.rows[0]["func_adminloginwhentoken"].data });
    } catch (error) {
      // reject(error);
      resolve({ status: false, code: 255, message: "Error System" });
    }
  });
};

function verifyToken(token) {
  return new Promise((resolve, reject) => {
    if (!token) {
      resolve({ status: 401, message: "Token không được cung cấp" });
    }

    jwt.verify(token, SECRET_KEY, async (err, decoded) => {
      if (err) {
        console.error("Lỗi xác thực token:", err);
        resolve({ status: false, message: "Token không hợp lệ" });
      } else {
        try {
          const userResult = await LoginWhenIsToken(decoded.name);
          if (userResult.status) {
            resolve({ status: true, username: userResult.username });
          } else {
            resolve({ status: false, message: userResult.message });
          }
        } catch (err) {
          console.error("Lỗi hệ thống:", err);
          resolve({ status: false, code: 255, message: "Error System" });
        }
      }
    });
  });
}

const changePassword = async (body) => {
  return new Promise(async (resolve, reject) => {
    try {
      console.log(body);
      let res = await db.SELECT(
         "*",
        "func_adminlogin('" + body[0] + "')"
      );
      if (res.rows.length == 0) resolve({ status: false, message: "không tìm thấy tài khoản" });
      bcrypt.compare(body[1],res.rows[0].password, async (err, result) => {
        if (result) {
          body[1] = await bcrypt.hash(body[2], saltRounds);
          body.splice(2, 1);
          console.log(body)
          let reschange = await db.SELECT(
            "*",
            "func_adminchangepassword",body
          );
          console.log(reschange.rows[0])
          if (reschange.rows[0]["func_adminchangepassword"].status == true)
            {
              resolve({ status: true, message: reschange.rows[0]["func_adminchangepassword"].message });
            }
            else resolve({ status: false, message: "Đổi mật khẩu thất bại" });
        } else resolve({ status: false, message: "Sai mật khẩu" });
    }); 
      
    } catch (error) {
      resolve({ status: false, code: 255, message: "Error System" });
    }
  });
};

module.exports = {
  checkValidAdmin,
  changePassword,
  verifyToken,
};
